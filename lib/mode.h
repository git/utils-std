// Collection of Unix tools, comparable to coreutils
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#include <sys/stat.h> // mode_t

// Returns 0 on error and sets errstr
mode_t new_mode(const char *mode, mode_t old, const char **errstr);

// Octal file mode in `mode`, symbolic version in `str`
// Example: mode=040755 ; str="drwxr-xr-x"
void symbolize_mode(mode_t mode, char str[11]);
