// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#include "../config.h" // HAS_*

#include <stdbool.h>
#include <stddef.h> // size_t
#include <stdio.h>  // off_t

// barebones basename, returns what's after the rightmost slash
char *static_basename(char *path);

// Modifies a path in-place to given a parent (dirname-like) and a child (basename-like)
// optionally trims the tailing slashes prior to splitting
char *path_split_static(char *path, bool trim);

// Copy data from fd_in to fd_out, with a max of len (flag is ignored for now)
ssize_t manual_file_copy(int fd_in, int fd_out, off_t len, int flags);

#ifndef HAS_COPY_FILE_RANGE
#define auto_file_copy manual_file_copy
#else
// Copy data with copy_file_range(2) for <len> amount of data, with fallback to manual_file_copy when cross-device
ssize_t auto_file_copy(int fd_in, int fd_out, off_t len, int flags);
#endif

#ifndef HAS_SENDFILE
#define auto_fd_copy(fd_in, fd_out, len) auto_file_copy(fd_in, fd_out, len, 0)
#else
ssize_t auto_fd_copy(int fd_in, int fd_out, size_t len);
#endif

// lib/offline_realpath.c
char *offline_realpath(const char *restrict filename, char *restrict resolved);
