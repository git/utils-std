// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#include <stdint.h>    // uint8_t
#include <sys/types.h> // size_t

void bytes2hex(const uint8_t *data, size_t datalen, char *buf, size_t buflen);
