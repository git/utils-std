// Collection of Unix tools, comparable to coreutils
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L

#include "err.h"

#include <stdio.h>
#include <stdlib.h>

void
utils_vwarn(const char *fmt, va_list ap)
{
	fprintf(stderr, "%s: warning: ", argv0);
	if(fmt)
	{
		vfprintf(stderr, fmt, ap);
		fputs(": ", stderr);
	}
	perror(NULL);
}

void
utils_vwarnx(const char *fmt, va_list ap)
{
	fprintf(stderr, "%s: warning: ", argv0);
	if(fmt) vfprintf(stderr, fmt, ap);
	putc('\n', stderr);
}

_Noreturn void
utils_verr(int status, const char *fmt, va_list ap)
{
	fprintf(stderr, "%s: error: ", argv0);
	if(fmt)
	{
		vfprintf(stderr, fmt, ap);
		fputs(": ", stderr);
	}
	perror(NULL);
	exit(status);
}

_Noreturn void
utils_verrx(int status, const char *fmt, va_list ap)
{
	fprintf(stderr, "%s: error: ", argv0);
	if(fmt) vfprintf(stderr, fmt, ap);
	putc('\n', stderr);
	exit(status);
}

void
utils_warn(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	utils_vwarn(fmt, ap);
	va_end(ap);
}

void
utils_warnx(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	utils_vwarnx(fmt, ap);
	va_end(ap);
}

_Noreturn void
utils_err(int status, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	utils_verr(status, fmt, ap);
	va_end(ap);
}

_Noreturn void
utils_errx(int status, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	utils_verrx(status, fmt, ap);
	va_end(ap);
}
