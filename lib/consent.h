// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#include <stdbool.h>

extern const char *argv0;

// Consent therefore defaults to no, including in cases of error
bool consentf(const char *restrict fmt, ...);
void consent_finish(void);
void consent_init(void);
