// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#include <stdbool.h> // bool
#include <stddef.h>  // size_t

struct si_scale
{
	double number;
	const char *prefix;
	unsigned exponent;
};

// dtosi(num, iec) => num ≈ number*scale^exponent where scale == iec ? 1024 : 1000
// SI scale returns prefix values like k, M, G, …
// IEC scale returns prefix values like KiB, MiB, GiB, …
struct si_scale dtosi(double num, bool iec);
