// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: CC0-1.0 OR WTFPL

// unsigned by definition reclaims the sign bit of their associated
// signed representation, meaning you can always represent the absolute
// values in an unsigned, even in two's complement where keeping
// a signed representation would cause an error.
//
// Sadly the abs() family of functions in <stdint.h> is misdesigned

extern unsigned int
absu(int a)
{
	if(a >= 0)
		return (unsigned int)a;
	else
		return (unsigned int)-a;
}

extern long unsigned int
labsu(long int a)
{
	if(a >= 0)
		return (long unsigned int)a;
	else
		return (long unsigned int)-a;
}

extern long long unsigned int
llabsu(long long int a)
{
	if(a >= 0)
		return (long long unsigned int)a;
	else
		return (long long unsigned int)-a;
}
