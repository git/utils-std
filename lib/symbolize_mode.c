// Collection of Unix tools, comparable to coreutils
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L
#define _XOPEN_SOURCE 700 // S_ISVTX, S_IFMT, S_IFDIR, …
#include "bitmasks.h"
#include "mode.h"

#include <sys/stat.h>

void
symbolize_mode(mode_t mode, char str[11])
{
	switch(mode & S_IFMT)
	{
	case S_IFDIR:
		str[0] = 'd';
		break;
	case S_IFCHR:
		str[0] = 'c';
		break;
	case S_IFBLK:
		str[0] = 'b';
		break;
	case S_IFREG:
		str[0] = '-';
		break;
	case S_IFIFO:
		str[0] = 'f';
		break;
	case S_IFLNK:
		str[0] = 'l';
		break;
	case S_IFSOCK:
		str[0] = 's';
		break;
	default:
		str[0] = '?';
		break;
	}

	str[1] = FIELD_MATCH(mode, S_IRUSR) ? 'r' : '-';
	str[2] = FIELD_MATCH(mode, S_IWUSR) ? 'w' : '-';
	if(FIELD_MATCH(mode, S_ISUID))
		str[3] = FIELD_MATCH(mode, S_IXUSR) ? 's' : 'S';
	else
		str[3] = FIELD_MATCH(mode, S_IXUSR) ? 'x' : '-';

	str[4] = FIELD_MATCH(mode, S_IRGRP) ? 'r' : '-';
	str[5] = FIELD_MATCH(mode, S_IWGRP) ? 'w' : '-';
	if(FIELD_MATCH(mode, S_ISGID))
		str[6] = FIELD_MATCH(mode, S_IXGRP) ? 's' : 'S';
	else
		str[6] = FIELD_MATCH(mode, S_IXGRP) ? 'x' : '-';

	str[7] = FIELD_MATCH(mode, S_IROTH) ? 'r' : '-';
	str[8] = FIELD_MATCH(mode, S_IWOTH) ? 'w' : '-';
	if(FIELD_MATCH(mode, S_ISVTX))
		str[9] = FIELD_MATCH(mode, S_IXOTH) ? 't' : 'T';
	else
		str[9] = FIELD_MATCH(mode, S_IXOTH) ? 'x' : '-';

	str[10] = '\0';
}
