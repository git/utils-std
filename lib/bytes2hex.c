// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#define _POSIX_C_SOURCE 200809L
#include "./strconv.h"

#include <assert.h>

void
bytes2hex(const uint8_t *data, size_t datalen, char *buf, size_t buflen)
{
	static const char *hextab = "0123456789abcdef";

	assert(buflen >= datalen * 2);

	size_t bi = 0;
	for(size_t i = 0; i < datalen; i++)
	{
		buf[bi++] = hextab[data[i] / 0x10];
		buf[bi++] = hextab[data[i] % 0x10];
	}
	if(bi < buflen) buf[bi] = '\0';
}
