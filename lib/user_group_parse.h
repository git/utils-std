// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L

#include <sys/types.h> // uid_t, gid_t

extern const char *argv0;

int parse_user(char *str, uid_t *user);
int parse_group(char *str, gid_t *group);
