// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#include <stdbool.h>
#include <sys/types.h> // mode_t

// to be declared in each utility
extern const char *argv0;
extern bool mkdir_parents_verbose;
extern mode_t mkdir_parents_filemask;

int mkdir_parents(char *path, mode_t mode);
