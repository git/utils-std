// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: CC0-1.0 OR WTFPL
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE // NSIG
#endif

#include <signal.h> // NSIG

// POSIX defect 741 (applied in issue 8 draft 4.1) adds _SC_NSIG
// https://www.austingroupbugs.net/view.php?id=741
// But it isn't implemented in current (musl 1.2.5, glibc 2.38) systems,
// so I'm keeping it this way for now.
#ifndef NSIG
#if defined(SIGMAX)
#define NSIG (SIGMAX + 1)
#else
#warning NSIG nor SIGMAX were defined, this might be a bug.
#define NSIG (8 * sizeof(sigset_t) + 1)
#endif
#endif

extern const char *util_sys_signame[NSIG];

static size_t util_sys_signame_len = NSIG;
