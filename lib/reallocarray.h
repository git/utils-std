// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#if !defined(_POSIX_C_SOURCE) || _POSIX_C_SOURCE < 202405L
#error reallocarray needs: define _POSIX_C_SOURCE 202405L
#endif

// pre-POSIX.1-2024 reallocarray fallback
#if !defined(_POSIX_VERSION) || _POSIX_VERSION < 202405L
#define _BSD_SOURCE
// FreeBSD
#undef _POSIX_C_SOURCE
#if __NetBSD_Version__ < 1000000000
#define _OPENBSD_SOURCE
#endif
#endif

#include <stdlib.h> // reallocarray
