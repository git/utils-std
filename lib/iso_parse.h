// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#include <time.h> /* struct tm */

// Sets errstr on failure
// YYYY-MM-DD[T ]hh:mm:SS([,\.]frac)?(Z|[+\-]hh:?mm)?
extern char *iso_parse(char *arg, struct tm *time, long *nsec, const char **errstr);

// Because mktime() messes with tm_gmtoff yet doesn't applies the offset
// Returns (time_t)-1 on failure
extern time_t mktime_tz(struct tm *tm);
