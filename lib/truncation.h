// Collection of Unix tools, comparable to coreutils
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L

#include <sys/types.h> // off_t

enum operation_e
{
	OP_SET,
	OP_INC,
	OP_DEC,
	OP_CHK_DOWN,
	OP_CHK_UP,
};

struct truncation
{
	enum operation_e op;
	off_t size;
};

int parse_size(const char *arg, struct truncation *buf);
int apply_size_suffix(unsigned long *size, char *endptr);
int apply_truncation(int fd, struct truncation tr, char *arg);
