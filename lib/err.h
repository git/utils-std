// Collection of Unix tools, comparable to coreutils
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#include <stdarg.h>

extern const char *argv0;

void utils_vwarn(const char *fmt, va_list ap);
void utils_vwarnx(const char *fmt, va_list ap);

_Noreturn void utils_verr(int status, const char *fmt, va_list ap);
_Noreturn void utils_verrx(int status, const char *fmt, va_list ap);

void utils_warn(const char *fmt, ...);
void utils_warnx(const char *fmt, ...);

_Noreturn void utils_err(int status, const char *fmt, ...);
_Noreturn void utils_errx(int status, const char *fmt, ...);
