// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#include <time.h> // timespec

extern const char *argv0;

int strtodur(char *s, struct timespec *dur);
