// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: CC0-1.0

// C had one job: Portable bitfields.
#define FIELD_SET(field, val) field |= (val)
#define FIELD_CLR(field, val) field &= ~(val)
#define FIELD_TGL(field, val) field ^= (val)

#define FIELD_GET(field, val) (field & (val))

#define FIELD_MATCH(field, val) ((field & (val)) == (val))
