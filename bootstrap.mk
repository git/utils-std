# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

# Reduced Makefile dedicated to building executables for ./configure

CC ?= cc

# List of commands used by ./configure
#
# Can be checked with:
# $ strace -o configure.strace -f -e execve ./configure
# $ grep -F -e 'execve(' configure.strace | cut -f2 -d '"' | xargs -n1 basename | sort -u
#all: cmd/cat cmd/echo cmd/grep cmd/printf cmd/rm cmd/sed cmd/test cmd/tr
all: cmd/cat cmd/echo cmd/printf cmd/rm cmd/test cmd/tr
	@./cmd/printf '%s\n' 'Done, you should now be able to run PATH=cmd/:$$PATH ./configure && make clean && make'

config.h:
	: >config.h

.c:
	$(CC) -std=c99 $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LDSTATIC)

.c.o:
	$(CC) -std=c99 $(CFLAGS) -c -o $@ $<

include common.mk
