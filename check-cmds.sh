#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(realpath "$(dirname "$0")")"
log="${WD}/check-cmds.log"
err=0
. "${WD}/check-funcs.sh"

cd "$WD"

rm "$log"

for runner in test-cmd/*.sh
do
	[ "$runner" = "test-cmd/tap.sh" ] && continue
	[ "$runner" = "test-cmd/init_env.sh" ] && continue
	wrap_test "${runner}"
done

[ $err = 0 ] && rm "$log"
exit $err
