# utils-std: Collection of commonly available Unix tools
<!--
Licensing of README file:
Copyright 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
SPDX-License-Identifier: MPL-2.0
-->

This is my own collection of commonly available Unix utilities, most of them implemented by me.
POSIX is used as a guide, but current Unix systems are the biggest inspiration and compatibility with portable scripts is intended.

Developed on Linux+musl, automatically tested on FreeBSD, NetBSD and ArchLinux thanks to [SourceHut](https://builds.sr.ht/~lanodan/utils-std).

## Dependencies
- C99 Compiler + POSIX.1-2017 C Library (POSIX.1-2024 recommended)
- POSIX yacc(1) implementation
- POSIX Shell
- POSIX.1-2024 Make
- (optional, test) [prysk](https://www.prysk.net/) (or [cram](https://bitheap.org/cram/) but it got abandoned)
- (optional, lint) mandoc: <https://mdocml.bsd.lv/> For linting the manual pages

## Packaging
- The `./configure` script isn't auto*hell based, you can pass it arguments via key-value arguments or environment variables
- You can define the `NO_BWRAP` environment variable to skip the bwrap-based tests which can cause issues in some environments like Gentoo `sandbox`
- If you want statically linked executables, pass `LDSTATIC=-static` to configure (environment or argument)
- `make -f bootstrap.mk` will compile the few utilities used by `./configure` for you

## Cross-compiling
Set the `CC` and `CFLAGS` environment variables for your target, the usual `CROSS_COMPILE` is also supported as a fallback.  
To compile the test binaries without running them, run `make build-checks`

## Licensing

The default license for new original works is the `MPL-2.0`, except when they should be broadly distributed and proprietary extensions aren't a concern (so `BSD-3-Clause`), or are considered under the copyright threshold (so `0BSD`).

When works are imported from other codebases they should be either under a permissive license or `MPL-2.0` and their licensing is kept to allow the original authors to use modifications made if any (copyleft in spirit).

## Design differences
- Whenever possible programs prefix their messages with their name. This way you don't end up with context-less messages.
- Error messages contain "error:", warning messages contain "warning:". To allow to easily find what went wrong in logs.
- When the output is structured data, a terminal gets human-oriented output, others like piped programs get machine-oriented output (no padding, line separation, CSV, JSON, …). Prior art: most `ls(1)` implementations, i3/sway, FreeBSD utils via `libxo`, …
- As the `utmp` and `wtmp` files are considered broken by design, utilities dependent on them like `who` and `lastlog` won't be implemented. Proper authentication logs shouldn't be writable into by multiple programs, some of them third-party (like SSH and Display Managers typically are).
- Minimal dependencies, allowing to bootstrap utils-std with barely having unix utilities.
- No binary blobs, even in the testsuite. Output of generated code is itself auditable.

### Difference with coreutils

Straightforward code, similarly to glibc, coreutils codebase often has it's actual code hidden under layers of macros and wrapper functions.

### Difference with BusyBox/ToyBox
Instead of minimalism as a constraint, here it is seen as a virtue of good design and as such as a process/long-term-goal.
But being a near drop-in replacement is utils-std reason for existing.

Also Toybox is stuck with reimplementing everything free of copyright issues due to it's choice of 0BSD.  
utils-std default license is the MPL-2.0 to be able to take code and design back without legal issues, but also is able to take code from other implementations.

## Goals
- (long-run) Effectively serve as one of the blocks to get base utilities, replacing GNU coreutils and part of BusyBox/ToyBox
- Code readability and hackability
- Portability, should run on modern libre POSIX systems as to not introduce vendor-locking
- Efficiency, but not without sacrificing the other goals

## Non-Goals
- Reimplementing complex utilities that already have great code or ought to be maintained separately
- Implementing utilities which aren't in your usual base system. This is covered by [utils-extra](https://hacktivis.me/git/utils-extra), and I'd also recommend checking out [moreutils](https://joeyh.name/code/moreutils/)
- Reimplementing OS-specific utilities like [util-linux](https://www.kernel.org/pub/linux/utils/util-linux/)
