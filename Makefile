# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

include config.mk

# Commands implemented as scripts
SCRIPTS=
# Commands linking to another executable
SYMLINKS=cmd/'[' cmd/chgrp cmd/readlink

RM=rm

all: $(EXE) $(MAN1SO)

.c:
	$(RM) -f ${<:=.gcov} ${@:=.gcda} ${@:=.gcno}
	$(CC) -std=c99 $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LDSTATIC)

.c.o:
	$(RM) -f ${<:=.gcov} ${@:=.gcda} ${@:=.gcno}
	$(CC) -std=c99 $(CFLAGS) -c -o $@ $<

include common.mk

# selfcheck: Check without extra dependencies
.PHONY: selfcheck
selfcheck: selfcheck-cmds selfcheck-libs

.PHONY: selfcheck-cmds
TEST_CMDS = test-cmd/pathchk-getlimits test-cmd/getpriority
selfcheck-cmds: $(EXE) $(TEST_CMDS)
	LDSTATIC="$(LDSTATIC)" ./check-cmds.sh

.PHONY: selfcheck-libs
TEST_LIBS = test-lib/t_mode test-lib/t_strtodur test-lib/t_symbolize_mode test-lib/t_truncation test-lib/t_sha1 test-lib/t_sha256 test-lib/t_sha512
selfcheck-libs: $(TEST_LIBS)
	LDSTATIC="$(LDSTATIC)" ./check-libs.sh $(TEST_LIBS)

# Manpages with examples that cram/prysk can grok
MAN_EXAMPLES = cmd/readlink.1
.PHONY: check
check: all check-man selfcheck-libs selfcheck-cmds
	MALLOC_CHECK_=3 POSIX_ME_HARDER=1 POSIXLY_CORRECT=1 LC_ALL=C.UTF-8 $(CRAM) test-cmd/*.t ${MAN_EXAMPLES}

.PHONY: check_status
check_status:
	grep EXE config.mk | sed -e 's;^EXE = ;;' -e 's;cmd/;;g' | tr ' ' '\n' | grep -v '^$$' | sed -e 's;^;^;' -e 's;$$;\\b;' | grep -f - *.txt | grep -iv done

.PHONY: check-man
check-man: $(MAN1SO)
	err=0; for i in $(EXE); do if test ! -f $$i.1 && test ! -f $$i.1.in; then echo "$$i: missing manpage"; err=1; fi; done; exit $$err
	for i in $(MAN1) ; do grep -q 'EXIT STATUS' $$i || echo "$i lacks EXIT STATUS"; grep -q -e 'STANDARDS' -e 'HISTORY' $$i || echo "$i lacks STANDARDS/HISTORY"; done

.PHONY: build-checks
build-checks: all $(TEST_CMDS) $(TEST_LIBS)

.PHONY: lint
lint: $(MAN1SO)
	$(MANDOC) -Tlint -Wunsupp,error,warning $(MAN1)
	$(REUSE) lint --quiet

.PHONY: clean
clean:
	$(RM) -fr $(EXE) ${EXE:=.c.gcov} ${EXE:=.gcda} ${EXE:=.gcno} cmd/expr.tab.c $(MAN1SO) $(TEST_CMDS) $(TEST_LIBS) ${LIBUTILS_O}

install: all
	mkdir -p ${DESTDIR}${BINDIR}/
	cp -pP ${EXE} ${SCRIPTS} ${SYMLINKS} ${DESTDIR}${BINDIR}/
	mkdir -p ${DESTDIR}${MANDIR}/man1
	cp -pP ${MAN1} ${DESTDIR}${MANDIR}/man1

.PHONY: coverage
coverage:
	$(GCOV) -b $(EXE)

C_SOURCES = cmd/*.c lib/*.h lib/*.c test-lib/*.c configure.d/*.c
format: $(C_SOURCES)
	clang-format -style=file -assume-filename=.clang-format -i $(C_SOURCES)

lib/sys_signame.c: lib/sys_signame.sh
	lib/sys_signame.sh >|lib/sys_signame.c

LIBUTILS_O = ${LIBUTILS_C:.c=.o}
lib/utils.a: ${LIBUTILS_O} ${LIBUTILS_H} config.mk config.h Makefile
	${AR} rc ${ARFLAGS} lib/utils.a ${LIBUTILS_O}

build/cmd/date.1: cmd/date.1.in lib/iso_parse.mdoc Makefile
	$(M4) cmd/date.1.in > $@

build/cmd/touch.1: cmd/touch.1.in lib/iso_parse.mdoc Makefile
	$(M4) cmd/touch.1.in > $@

cmd/expr.tab.c: cmd/expr.y Makefile
	$(YACC) -b cmd/expr cmd/expr.y

# Needs -D_POSIX_C_SOURCE=200809L due to OpenBSD yacc
cmd/expr: cmd/expr.tab.c lib/utils.a
	$(RM) -f cmd/expr.tab.c.gcov cmd/expr.tab.o.gcda cmd/expr.tab.o.gcno
	$(CC) -std=c99 $(CFLAGS) -D_POSIX_C_SOURCE=200809L -o cmd/expr cmd/expr.tab.c lib/utils.a $(LDFLAGS) $(LDSTATIC)

# Needs -lm
cmd/seq: cmd/seq.c Makefile
	$(RM) -f ${<:=.gcov} ${@:=.gcda} ${@:=.gcno}
	$(CC) -std=c99 $(CFLAGS) -o $@ cmd/seq.c -lm $(LDFLAGS) $(LDSTATIC)

cmd/chmod: cmd/chmod.c lib/utils.a
cmd/chown: cmd/chown.c lib/utils.a
cmd/date: cmd/date.c lib/utils.a
cmd/df: cmd/df.c lib/utils.a
cmd/head: cmd/head.c lib/utils.a
cmd/install: cmd/install.c lib/utils.a
cmd/join: cmd/join.c lib/utils.a
cmd/mkdir: cmd/mkdir.c lib/utils.a
cmd/mkfifo: cmd/mkfifo.c lib/utils.a
cmd/mknod: cmd/mknod.c lib/utils.a
cmd/mv: cmd/mv.c lib/utils.a
cmd/paste: cmd/paste.c lib/utils.a
cmd/realpath: cmd/realpath.c lib/utils.a
cmd/seq: cmd/seq.c lib/utils.a
cmd/sha1sum: cmd/sha1sum.c lib/utils.a
cmd/sha256sum: cmd/sha256sum.c lib/utils.a
cmd/sha512sum: cmd/sha512sum.c lib/utils.a
cmd/sleep: cmd/sleep.c lib/utils.a
cmd/split: cmd/split.c lib/utils.a
cmd/timeout: cmd/timeout.c lib/utils.a
cmd/touch: cmd/touch.c lib/utils.a
cmd/truncate: cmd/truncate.c lib/utils.a
cmd/whoami: cmd/whoami.c lib/utils.a

lib/mode.o: lib/mode.c lib/mode.h
lib/sha1.o: lib/sha1.c lib/sha1.h
lib/sha256.o: lib/sha256.c lib/sha256.h
lib/sha512.o: lib/sha512.c lib/sha512.h
lib/bytes2hex.o: lib/bytes2hex.c lib/strconv.h

test-lib/t_mode: test-lib/t_mode.c lib/mode.o
test-lib/t_strtodur: test-lib/t_strtodur.c lib/strtodur.o
test-lib/t_symbolize_mode: test-lib/t_symbolize_mode.c lib/symbolize_mode.o
test-lib/t_truncation: test-lib/t_truncation.c lib/truncation.o
test-lib/t_sha1: test-lib/t_sha1.c lib/sha1.c lib/bytes2hex.o
test-lib/t_sha256: test-lib/t_sha256.c lib/sha256.c lib/bytes2hex.o
test-lib/t_sha512: test-lib/t_sha512.c lib/sha512.c lib/bytes2hex.o
