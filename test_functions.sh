#!/bin/sh

# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

has_glibc() {
	test -f /usr/include/features.h && grep -q '#define\W__GLIBC__' /usr/include/features.h
}

has_musl() {
	command -v ldd && ldd 2>&1 | grep -q musl
}

gentoo_sandbox() {
	test -z "${LDSTATIC}" -a "${LD_PRELOAD}" = "libsandbox.so"
}
