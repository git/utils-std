#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: 0BSD

WD=$(dirname "$0")
target="${WD}/../cmd/df"
plans=7
. "${WD}/tap.sh"

# stderr ignored because not all filesystems allow statvfs from a normal user.
# For example tracefs aka "/sys/kernel/debug/tracing" on Linux.
t_df_l1() {
	"$target" "$@" 2>/dev/null | head -n 1
}
t_cmd posix 'Filesystem 512-blocks Used Available Capacity Mounted on
' t_df_l1 -P
t_cmd posix_1k 'Filesystem 1024-blocks Used Available Capacity Mounted on
' t_df_l1 -Pk

set -o pipefail

t_df_posix_cols() {
	"$target" "$@" 2>/dev/null | sed -n '2,$p' | { grep -vE '[^ ]+ (-|[0-9]+) (-|[0-9]+) (-|[0-9]+) (-|[0-9]+%) [^ ]+$'; return 0; }
}
t_cmd posix_cols '' t_df_posix_cols -P
t_cmd posix_cols_k '' t_df_posix_cols -Pk
t_cmd posix_cols_a '' t_df_posix_cols -Pa
t_cmd posix_cols_ak '' t_df_posix_cols -Pak

t_df_rootfs() {
	"$target" "$@" 2>/dev/null | sed -n '2,$p' | { grep -vE '^[^ ]+ [0-9]+ [0-9]+ [0-9]+ [0-9]+% /$'; return 0; }
}
t_cmd posix_cols_rootfs '' t_df_rootfs -P /
