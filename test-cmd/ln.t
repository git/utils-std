#!/usr/bin/env cram
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

  $ export PATH="$TESTDIR/../cmd:$PATH"

  $ test "$(command -v ln)" = "$TESTDIR/../cmd/ln"

  $ ln hard_enoent_src hard_enoent_dest
  ln: error: Failed creating hard link from 'hard_enoent_src' to 'hard_enoent_dest': No such file or directory
  [1]

  $ ln -s hard_enoent_ref1 hard_enoent_ref2 hard_enoent_dest
  ln: error: Failed creating symlink 'hard_enoent_dest/hard_enoent_ref1': No such file or directory
  [1]

  $ touch hard_file_src
  $ ln hard_file_src hard_file_dest
  $ test -f hard_file_dest
  $ rm hard_file_src hard_file_dest

  $ touch hard_file_src1 ./hard_file_src2
  $ mkdir hard_dir_dest
  $ ln hard_file_src1 ./hard_file_src2 hard_dir_dest
  $ test -f hard_dir_dest/hard_file_src1
  $ test -f hard_dir_dest/hard_file_src2
  $ rm hard_file_src1 hard_file_src2
  $ rm -r hard_dir_dest

  $ ln -s sym_enoent_ref sym_enoent_dest
  $ readlink sym_enoent_dest
  sym_enoent_ref
  $ rm sym_enoent_dest

  $ ln -s sym_enoent_ref1 sym_enoent_ref2 sym_enoent_dest
  ln: error: Failed creating symlink 'sym_enoent_dest/sym_enoent_ref1': No such file or directory
  [1]

  $ mkdir sym_dir_slash
  $ ln -s sym_enoent_ref1 ./sym_enoent_ref2 sym_dir_slash/
  $ readlink sym_dir_slash/sym_enoent_ref1
  sym_enoent_ref1
  $ readlink sym_dir_slash/sym_enoent_ref2
  ./sym_enoent_ref2
  $ rm -r sym_dir_slash

  $ mkdir sym_dir_noslash
  $ ln -s sym_enoent_ref1 ./sym_enoent_ref2 sym_dir_noslash
  $ readlink sym_dir_noslash/sym_enoent_ref1
  sym_enoent_ref1
  $ readlink sym_dir_noslash/sym_enoent_ref2
  ./sym_enoent_ref2
  $ rm -r sym_dir_noslash

  $ touch force_symlink
  $ ln -s foo force_symlink
  ln: error: Destination 'force_symlink' already exists
  [1]
  $ test -L force_symlink
  [1]
  $ ln -sf foo force_symlink
  $ test -L force_symlink
  $ rm force_symlink

  $ mkdir n_directory
  $ ln -s n_directory n_dir_symlink
  $ ln -sn //example.org n_dir_symlink
  ln: error: Destination 'n_dir_symlink' already exists
  [1]
  $ readlink n_dir_symlink
  n_directory
  $ ln -snf //example.org n_dir_symlink
  $ readlink n_dir_symlink
  //example.org
  $ rm -r n_directory n_dir_symlink

  $ mkdir e_target_dir
  $ ln -s e_ref_dir e_target_dir
  $ test ! -e e_ref_dir
  $ test ! -L e_target_dir
  $ test -d e_target_dir
  $ test -L e_target_dir/e_ref_dir
  $ echo mere copy > e_src_dir
  $ test -f e_src_dir
  $ ln e_src_dir e_target_dir
  $ echo hardlink > e_src_dir
  $ test -d e_target_dir
  $ test -f e_target_dir/e_src_dir
  $ cat e_target_dir/e_src_dir
  hardlink
  $ rm -r e_target_dir e_src_dir

  $ ln -sn //example.org
  $ readlink ./example.org
  //example.org
  $ rm ./example.org

  $ find .
  .
