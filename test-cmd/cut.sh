#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
target="${WD}/cmd/cut"
plans=15
. "${WD}/test-cmd/tap.sh"
. "${WD}/test-cmd/init_env.sh"

t_null_f2()
{
	name="null_f2"
	exp="_2"

	count=$((count+1))
	out="$("$target" -d '' -f2 "${WD}/test-cmd/inputs/strings/length" 2>&1)"
	ret="$?"
	if [ "$?" != 0 ]; then
		printf 'not ok %s - %s\n' "$count $name" "$out"
	elif [ "$out" != "$exp" ]; then
		printf 'not ok %s - (%s != %s)\n' "$count $name" "$out" "$exp"
	else
		printf 'ok %s\n' "$count $name"
	fi
}

t 'bytes:2-3,10-20,4,12' "-b 2-3,10-20,4,12 ${WD}/test-cmd/inputs/alnum" '1239ABCDEFGHIJ
'
t 'bytes:11-' "-b 11- ${WD}/test-cmd/inputs/alnum" 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
'
t 'bytes:-10' "-b -10 ${WD}/test-cmd/inputs/alnum" '0123456789
'
t --exit=1 'bytes:,' "-b , ${WD}/test-cmd/inputs/alnum" 'cut: error: Empty list element
'
# Example taken from POSIX cut(1)
t --input='abcdefghi' 'chars:6,2,4-7,1' '-c 6,2,4-7,1' 'abdefg
'

t 'chars:11-' "-c 11- ${WD}/test-cmd/inputs/alnum" 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
'
t 'chars:-10' "-c -10 ${WD}/test-cmd/inputs/alnum" '0123456789
'

t --input='aéb' 'widechar' '-c2' 'é
'

fields='1	2	3	4
A
a	b	c'

t --input="$fields" f2 '-f2' '2
A
b
'

t --input="$fields" s_f2 '-s -f2' '2
b
'

t --input="$fields" f2- '-f2-' '2	3	4
A
b	c
'
t --input="$fields" f-2 '-f-2' '1	2
A
a	b
'

t --input="hello" 'hello_f1' '-f1' 'hello
'
t --input="hello" 'hello_f1_s' '-f1 -s'

t_null_f2
