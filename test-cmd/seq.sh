#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

seq="$(dirname "$0")/../cmd/seq"

t ()
{
	# $1 -> $test expression
	# $2 -> expected output

	count=$((count+1))
	out="$("$seq" -s, -- $1 2>&1)"
	ret="$?"
	if [ "$?" != 0 ]; then
		printf 'not ok %s - %s\n' "$count $1" "$out"
	elif [ "$out" != "$2" ]; then
		printf 'not ok %s - (%s != %s)\n' "$count $1" "$out" "$2"
	else
		printf 'ok %s\n' "$count $1"
	fi
}

count=0

echo '1..20'

# One arg
t 1 '1'
t 5 '1,2,3,4,5'
t -1 '1,0,-1'
t -5 '1,0,-1,-2,-3,-4,-5'

# Two args
t '0 1' '0,1'
t '0 5' '0,1,2,3,4,5'
t '10 15' '10,11,12,13,14,15'
t '2 -2' '2,1,0,-1,-2'
t '-2 2' '-2,-1,0,1,2'

# Three args
t '0 1 1' '0,1'
t '0 1 5' '0,1,2,3,4,5'
t '10 1 15' '10,11,12,13,14,15'

t '0 2 5' '0,2,4'
t '10 2 15' '10,12,14'

t '0 1 -1' '0,-1'
t '0 1 -5' '0,-1,-2,-3,-4,-5'
t '-10 1 -15' '-10,-11,-12,-13,-14,-15'

t '0 -1 -1' '0,-1'

t '0 2 -5' '0,-2,-4'
t '-10 2 -15' '-10,-12,-14'
