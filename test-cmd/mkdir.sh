#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/mkdir"
target_chmod="${WD}/../cmd/chmod"
plans=15
. "${WD}/tap.sh"

t --exit=1 noargs '' 'mkdir: error: Missing operand
Usage: mkdir [-pv] [-m mode] path ...
'

umask 002

t_cmd foo:absent '' rm -f foo
t foo:mkdir foo
t_cmd foo:isdir '' test -d foo
t_cmd foo:rm '' rm -r foo

t_cmd enoent:absent '' test ! -e enoent
t --exit=1 foo:mkdir enoent/file "mkdir: error: Failed making directory 'enoent/file': No such file or directory
"
t_cmd enoent:still_absent '' test ! -e enoent

t_mkdir_parents() {
	t_err=0

	test ! -e gaia || return 1
	"${target}" -p gaia/zeus || t_err=1
	"${target_chmod}" -v + gaia gaia/zeus || t_err=1
	rm -r gaia || t_err=1

	return $t_err
}

t_cmd mkdir_parents "chmod: Permissions already set to 00775/drwxrwxr-x for 'gaia'
chmod: Permissions already set to 00775/drwxrwxr-x for 'gaia/zeus'
" t_mkdir_parents

t_mkdir_v() {
	t_err=0
	for dir; do
		test ! -e "$dir" || return 1
	done
	"${target}" -v "$@" || return 1
	for dir; do
		test -d "$dir" || t_err=1
	done
	rm -r "$@" || t_err=1

	return $t_err
}
t_cmd verbose "mkdir: Made directory: verbose1
mkdir: Made directory: verbose2
" t_mkdir_v verbose1 verbose2

t_cmd --exit=1 verbose_no_parents "mkdir: error: Failed making directory 'verbose_foo/verbose_bar': No such file or directory
" t_mkdir_v verbose_foo/verbose_bar

t_mkdir_mode() {
	t_err=0

	test ! -e "$2" || return 1
	"${target}" -m "$1" "$2" || t_err=1
	"${target_chmod}" -v + "$2" || t_err=1
	rm -r "$2" || t_err=1

	return $t_err
}
t_cmd plus_x "chmod: Permissions already set to 00775/drwxrwxr-x for 'plus_x'
" t_mkdir_mode +x plus_x

t_cmd go_minus_x "chmod: Permissions already set to 00764/drwxrw-r-- for 'go_minus_x'
" t_mkdir_mode go-x go_minus_x

t_cmd minus_x "chmod: Permissions already set to 00664/drw-rw-r-- for 'minus_x'
" t_mkdir_mode -x minus_x

t_mkdir_024() {
	t_err=0

	umask 024
	test ! -e zero_two || return 1
	"${target}" -p zero_two/four || t_err=1
	"${target_chmod}" -v + zero_two zero_two/four || t_err=1
	rm -r zero_two || t_err=1

	return $t_err
}

t_cmd 024 "chmod: Permissions already set to 00753/drwxr-x-wx for 'zero_two'
chmod: Permissions already set to 00753/drwxr-x-wx for 'zero_two/four'
" t_mkdir_024
