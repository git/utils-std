#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
target="${WD}/cmd/chmod"
plans=13
. "${WD}/test-cmd/tap.sh"

tmpfile="${TMPDIR-/tmp}/test_chmod_$(date +%s)"
ref_file="${TMPDIR-/tmp}/test_chmod_$(date +%s).ref"

touch "$tmpfile" || exit 1
touch "$ref_file" || exit 1

t '0' "-v 0 $tmpfile" "chmod: Permissions changed from 00644/-rw-r--r-- to 00000/---------- for '${tmpfile}'
"
t '00' "-v 00 $tmpfile" "chmod: Permissions already set to 00000/---------- for '${tmpfile}'
"
t '000' "-v 000 $tmpfile" "chmod: Permissions already set to 00000/---------- for '${tmpfile}'
"
t '0000' "-v 0000 $tmpfile" "chmod: Permissions already set to 00000/---------- for '${tmpfile}'
"

t '0444' "-v 0444 $tmpfile" "chmod: Permissions changed from 00000/---------- to 00444/-r--r--r-- for '${tmpfile}'
"

t '0777' "-v 0777 $tmpfile" "chmod: Permissions changed from 00444/-r--r--r-- to 00777/-rwxrwxrwx for '${tmpfile}'
"

t --exit=1 'invalid 0888' "-v 0888 $tmpfile" "chmod: error: Failed parsing mode '0888': contains digit outside of [0-7]
"

t 'mode:-w,+x' "-v -w,+x $tmpfile" "chmod: warning: (portability) Pass -- before a mode with a leading dash(-) to separate it from options
chmod: Permissions changed from 00777/-rwxrwxrwx to 00577/-r-xrwxrwx for '${tmpfile}'
"

t 'mode:-r' "-v -r $tmpfile" "chmod: warning: (portability) Pass -- before a mode with a leading dash(-) to separate it from options
chmod: Permissions changed from 00577/-r-xrwxrwx to 00133/---x-wx-wx for '${tmpfile}'
"

t '__mode:-x,+w' "-v -- -x,+w $tmpfile" "chmod: Permissions changed from 00133/---x-wx-wx to 00222/--w--w--w- for '${tmpfile}'
"

t '__mode:-w' "-v -- -w $tmpfile" "chmod: Permissions changed from 00222/--w--w--w- to 00022/-----w--w- for '${tmpfile}'
"

if grep -q HAS_GETOPT_LONG "${WD}/config.h"; then
	t ref_file "-v --reference $ref_file $tmpfile" "chmod: Permissions changed from 00022/-----w--w- to 00644/-rw-r--r-- for '${tmpfile}'
"
	t ref_file:repeat "-v --reference $ref_file $tmpfile" "chmod: Permissions already set to 00644/-rw-r--r-- for '${tmpfile}'
"
else
	skip ref_file 'Lacks getopt_long'
	skip ref_file:repeat 'Lacks getopt_long'
fi

rm -f "$tmpfile" "$ref_file" || exit 1
