#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/tty"
plans=1
. "$(dirname "$0")/tap.sh"

tty >/dev/null 2>/dev/null
case $? in
	0)
		t noargs '' "$(tty)
"
	;;
	1)
		t --exit=1 noargs '' 'not a tty
'
	;;
	*)
		skip noargs "system tty exited with $?"
	;;
esac
