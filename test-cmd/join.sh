#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
target="${WD}/cmd/join"
plans=3
. "${WD}/test-cmd/tap.sh"

t_args phones_fiction \
'!Name	Phone Number	Origin
Mary	+1 (415) 273-9164	Sneakers (1992 film)
Аквариум	+7 212-85-06	Russian band
Ann	555-0179	Frasier
' \
	-t '	' -a 1 -a 2 -e '(unknown)' -o 0,1.2,2.2 \
	"${WD}/test-cmd/inputs/join/phone.txt" \
	"${WD}/test-cmd/inputs/join/origin.txt"

t_args posix_ax_az_ap \
'a x p
a y p
a z p
' \
	"${WD}/test-cmd/inputs/join/ax-az" \
	"${WD}/test-cmd/inputs/join/ap"

t_args posix_ac_ae_ap \
'a b c w x
a b c y z
a b c o p
a d e w x
a d e y z
a d e o p
' \
	"${WD}/test-cmd/inputs/join/abc-ade" \
	"${WD}/test-cmd/inputs/join/awx-aop"
