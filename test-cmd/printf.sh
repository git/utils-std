#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/printf"
plans=4
. "$(dirname "$0")/tap.sh"

t esc '\b\t\n' '	
'

t octal '\041' '!'
t hex '\x7B\x7d' '{}'

t repeat_fmt '%s\n foo bar' 'foo
bar
'
