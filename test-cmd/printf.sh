#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
target="${WD}/cmd/printf"
plans=6
. "${WD}/test-cmd/tap.sh"

t esc '\b\t\n' '	
'

t octal '\041' '!'
t hex '\x7B\x7d' '{}'

t repeat_fmt '%s\n foo bar' 'foo
bar
'

var_c_upper=$(cat <<'EOF'
\c@\cA\cB\cC\cD\cE\cF\cG\cH\cI\cJ\cK\cL\cM\cN\cO\cP\cQ\cR\cS\cT\cU\cV\cW\cX\cY\cZ\c[\c\\c]\c^\c_ !"#$%%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\c?
EOF
)

t_file esc_c_upper "${WD}/test-cmd/inputs/all_ascii" "${var_c_upper}"

var_c_lower=$(cat <<'EOF'
\c@\ca\cb\cc\cd\ce\cf\cg\ch\ci\cj\ck\cl\cm\cn\co\cp\cq\cr\cs\ct\cu\cv\cw\cx\cy\cz\c[\c\\c]\c^\c_ !"#$%%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\c?
EOF
)

t_file esc_c_lower "${WD}/test-cmd/inputs/all_ascii" "${var_c_lower}"
