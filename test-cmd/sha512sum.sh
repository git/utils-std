#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/sha512sum"
plans=16
. "$(dirname "$0")/tap.sh"

if test "$(uname -s)" = "FreeBSD"
then
	skip devnull 'FreeBSD treats posix_fadvise on /dev/null as invalid'
else
	t devnull '/dev/null' 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e  /dev/null
'
fi
t empty "$WD/inputs/empty" "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e  $WD/inputs/empty
"
t --input='' 'empty_stdin' '' 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e
'

t --input='abc' 'abc' '' 'ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f
'

t --exit=1 'enoent' '/var/empty/e/no/ent' "sha512sum: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"

t --input="cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e  $WD/inputs/empty" 'check:empty' '-c' "$WD/inputs/empty: OK
"
t --input="cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e *$WD/inputs/empty" 'bin_check:empty' '-c' "$WD/inputs/empty: OK
"
t --input="cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e *$WD/inputs/empty" 'bin_check:empty:dash' '-c -' "$WD/inputs/empty: OK
"

t --exit=1 --input="ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f  $WD/inputs/empty" 'xfail_check:empty' '-c' "$WD/inputs/empty: FAILED
"
t --exit=1 --input="ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f *$WD/inputs/empty" 'xfail_bin_check:empty' '-c' "$WD/inputs/empty: FAILED
"

t --exit=1 --input='cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e  /var/empty/e/no/ent' 'xfail_check:enoent' '-c' "sha512sum: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"
t --exit=1 --input='cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e */var/empty/e/no/ent' 'xfail_bin_check:enoent' '-c' "sha512sum: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"

t --exit=1 --input="# $WD/inputs/empty" 'invalid_chars:#' '-c' "sha512sum: error: Invalid character '#' while reading hash in line: # $WD/inputs/empty
"
t --exit=1 --input="cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e" 'missing_file' '-c' "sha512sum: error: Failed opening file '': No such file or directory
"
t --exit=1 --input="cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3  $WD/inputs/empty" 'truncated_hash' '-c' 'sha512sum: error: Got 128 hexadecimal digits while expected 129 for a SHA512
'
t --exit=1 --input="cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3ed  $WD/inputs/empty" 'elongated_hash' '-c' 'sha512sum: error: Got 130 hexadecimal digits while expected 129 for a SHA512
'
