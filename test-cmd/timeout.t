#!/usr/bin/env cram
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

  $ export PATH="$TESTDIR/../cmd:$PATH"

  $ test "$(command -v timeout)" = "$TESTDIR/../cmd/timeout"

Flaky on FreeBSD
  $ test "$(uname -s)" != "FreeBSD" || return 80

  $ timeout 0.5 sleep 10
  [124]
  $ timeout 0.5 sleep 0.2
  $ timeout 0.5 true
  $ timeout 0.5 false
  [1]

  $ timeout -s KILL 0.5 sleep 10
  [124]
  $ timeout -s SIGKILL 0.5 sleep 10
  [124]
  $ timeout -s 9 0.5 sleep 10
  [124]

  $ timeout -f 0.5 time -p sleep 1
  [124]
  $ timeout 0.5 time -p sleep 1
  real 1.\d+ (re)
  user 0.\d+ (re)
  sys 0.\d+ (re)
  [124]

  $ timeout -p 0.5 sleep 10
