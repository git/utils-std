#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/link"
plans=4
. "$(dirname "$0")/tap.sh"

t --exit=1 noargs '' 'usage: link <reference> <destination>
'

basic_foo=$(mktemp)
t basic "${basic_foo} ${basic_foo}.bar" ''
echo hello > "${basic_foo}" || exit 1
cmp "${basic_foo}" "${basic_foo}.bar" || exit 1
echo world > "${basic_foo}.bar" || exit 1
cmp "${basic_foo}" "${basic_foo}.bar" || exit 1
rm "${basic_foo}" "${basic_foo}.bar" || exit 1

t --exit=1 enoent1 '/var/empty/e/no/ent enoent_foo' 'link: error: No such file or directory
'
t --exit=1 enoent2 'enoent_foo /var/empty/e/no/ent' 'link: error: No such file or directory
'
