#!/usr/bin/env cram
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

  $ export PATH="$TESTDIR/../cmd:$PATH"

  $ test "$(command -v mv)" = "$TESTDIR/../cmd/mv"

Skip if /dev/shm doesn't exists
  $ test -d /dev/shm || exit 80

Successfully moves directory contents across filesystems
  $ mkdir non-empty-dir
  $ touch non-empty-dir/foo
  $ mkdir non-empty-dir/foo.d
  $ touch non-empty-dir/foo.d/bar
  $ test -f non-empty-dir/foo
  $ test -f non-empty-dir/foo.d/bar
  $ rm -f /dev/shm/mv-test-non-empty-dir
  $ mv non-empty-dir /dev/shm/mv-test-non-empty-dir
  $ test -f /dev/shm/mv-test-non-empty-dir/foo
  $ test -f /dev/shm/mv-test-non-empty-dir/foo.d/bar
  $ test ! -e non-empty-dir
  $ rm -fr /dev/shm/mv-test-non-empty-dir

No files should be left
  $ find .
  .
