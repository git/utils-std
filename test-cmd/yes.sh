#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/yes"
plans=5
. "${WD}/tap.sh"

yes_n3() { "$target" | head -n 3 ; }
t_cmd n3 'y
y
y
' yes_n3

yes_foo4() { "$target" foo | head -n 4 ; }
t_cmd foo4 'foo
foo
foo
foo
' yes_foo4

yes_empty4() { "$target" "" | head -n 4 ; }
t_cmd empty4 '



' yes_empty4

yes_foo6() { "$target" foo | head -n 6 ; }
t_cmd foo6 'foo
foo
foo
foo
foo
foo
' yes_foo6

yes_nl() { "$target" "$(printf 'foo\nbar')" | head -n 4 ; }
t_cmd newline 'foo
bar
foo
bar
' yes_nl
