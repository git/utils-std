#!/usr/bin/env cram
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

  $ export PATH="$TESTDIR/../cmd:$PATH"

  $ test "$(command -v truncate)" = "$TESTDIR/../cmd/truncate"

  $ touch foo

  $ truncate -s 0 foo
  $ wc -c foo
  \s*0\sfoo (re)

  $ truncate -s 666 foo
  $ wc -c foo
  \s*666\sfoo (re)

  $ truncate -s 666K foo
  $ wc -c foo
  \s*681984\sfoo (re)

  $ truncate -s 666KB foo
  $ wc -c foo
  \s*666000\sfoo (re)

  $ truncate -s 666KiB foo
  $ wc -c foo
  \s*681984\sfoo (re)

  $ truncate -s 1024 foo
  $ wc -c foo
  \s*1024\sfoo (re)
  $ truncate -s -666 foo
  $ wc -c foo
  \s*358\sfoo (re)

  $ truncate -s 666 foo
  $ wc -c foo
  \s*666\sfoo (re)
  $ truncate -s -1024 foo
  $ wc -c foo
  \s*0\sfoo (re)

  $ truncate -s 666 foo
  $ wc -c foo
  \s*666\sfoo (re)
  $ truncate -s +666 foo
  $ wc -c foo
  \s*1332\sfoo (re)

  $ truncate -s 15 foo
  $ wc -c foo
  \s*15\sfoo (re)
  $ truncate -s /10 foo
  $ wc -c foo
  \s*10\sfoo (re)

  $ truncate -s 15 foo
  $ wc -c foo
  \s*15\sfoo (re)
  $ truncate -s %10 foo
  $ wc -c foo
  \s*20\sfoo (re)

  $ truncate -s 15KB foo
  $ wc -c foo
  \s*15000\sfoo (re)
  $ truncate -s /10KB foo
  $ wc -c foo
  \s*10000\sfoo (re)

  $ truncate -s 15KB foo
  $ wc -c foo
  \s*15000\sfoo (re)
  $ truncate -s %10KB foo
  $ wc -c foo
  \s*20000\sfoo (re)
