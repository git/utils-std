#!/usr/bin/env cram
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

  $ cd $TESTDIR/../cmd

  $ ./time
  Usage: time [-p|-v] command [argument ...]

  $ ./time -f
  time: error: Unrecognised option: '-f'
  Usage: time [-p|-v] command [argument ...]
  [1]

  $ ./time /var/empty/e/no/ent
  time: error: Failed executing '/var/empty/e/no/ent': No such file or directory
  real 0.[0-9]* (re)
  user 0.[0-9]* (re)
  sys 0.[0-9]* (re)
  [127]

  $ ./time false
  real 0.[0-9]* (re)
  user 0.[0-9]* (re)
  sys 0.[0-9]* (re)
  [1]

  $ ./time true
  real 0.[0-9]* (re)
  user 0.[0-9]* (re)
  sys 0.[0-9]* (re)
