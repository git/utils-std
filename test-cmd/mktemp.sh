#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/mktemp"
plans=14
. "$(dirname "$0")/tap.sh"

t_mktemp()
{
	count=$((count+1))
	name="$1" ; shift

	# Append a final slash so sh(1) doesn't trims final newlines
	out=$("${target?}" "$@" 2>&1;r=$?;printf %s /;exit $r)
	ret=$?
	out=${out%/}

	test -f "${out%
}"
	test_ret="$?"

	rm -f "${out%
}"

	if [ "$ret" = 0 ] && [ "$test_ret" = 0 ]; then
		printf 'ok %d - %s\n' "$count" "$name"
	else
		printf 'not ok %d - %s\n' "$count" "$name"
		printf '# exit code: %d\n' "$ret"
		printf '# test -f => %d\n' "$test_ret"
		err=1
	fi
	printf "$out" | sed -e 's;^;# ;'
}

t_mkdtemp()
{
	count=$((count+1))
	name="$1" ; shift

	# Append a final slash so sh(1) doesn't trims final newlines
	out=$("${target?}" "$@" 2>&1;r=$?;printf %s /;exit $r)
	ret=$?
	out=${out%/}

	test -d "${out%
}"
	test_ret="$?"

	rm -fr "${out%
}"

	if [ "$ret" = 0 ] && [ "$test_ret" = 0 ]; then
		printf 'ok %d - %s\n' "$count" "$name"
	else
		printf 'not ok %d - %s\n' "$count" "$name"
		printf '# exit code: %d\n' "$ret"
		printf '# test -d => %d\n' "$test_ret"
		err=1
	fi
	printf "$out" | sed -e 's;^;# ;'
}

# TODO: Figure a way to do harsher tests, for example testing patterns, directory contents, …

t_mktemp noargs
t_mktemp template template.XXXXXX
t_mktemp tmpdir -t
t_mktemp tmpdir -t template.XXXXXX
t_mkdtemp dir -d
t_mkdtemp dir_template -d template.XXXXXX
t_mkdtemp dir_tmpdir -dt

t --exit=1 templ2 'foo bar' 'mktemp: error: Only one template argument is supported, got 2
'

cmd_mktemp_u() {
	if tmpfile=$("$target" "$@"); then
		if test -e "$tmpfile"; then
			printf '# Unexpectedly created a file:\n' "$tmpfile"
			ls -l "$tmpfile" 2>&1 | sed 's;^;# ;'
			rm -fr "$tmpfile" 2>&1 | sed 's;^;# ;'
			return 1
		fi
		return 0
	else
		return 1
	fi
}
t_cmd unsafe:file '' cmd_mktemp_u -u
t_cmd unsafe:file_template '' cmd_mktemp_u -u template.XXXXXX
t_args --exit=1 unsafe:file_template_line 'mktemp: error: Invalid character (newline) in template
' -u 'template
XXXXXX'

t_cmd unsafe:dir '' cmd_mktemp_u -u -d
t_cmd unsafe:dir_template '' cmd_mktemp_u -u -d template.XXXXXX
t_args --exit=1 unsafe:dir_template_line 'mktemp: error: Invalid character (newline) in template
' -u -d 'template
XXXXXX'
