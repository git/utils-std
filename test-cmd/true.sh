#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: 0BSD

target="$(dirname "$0")/../cmd/true"
plans=3
. "$(dirname "$0")/tap.sh"

t basic '' ''
t nohelp '--help' ''
t noversion '--version' ''
