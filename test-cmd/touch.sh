#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

if ! command -v stat >/dev/null 2>/dev/null; then
	echo '1..0 # SKIP: missing command: stat'
	exit 0
fi

WD=$(dirname "$0")
plans=33
target="${WD}/../cmd/touch"
. "${WD}/tap.sh"

tempfile="$(mktemp)"
tempdir="$(mktemp -d)"

t noargs

t file "$tempfile"

t ref_file:setup "-d 2007-11-12T10:15:30Z $tempfile"
t ref_file "-r $target $tempfile"
t_cmd ref_file:atime "$("${WD}/stat_atime" "$target")
" "${WD}/stat_atime" "$tempfile"
t_cmd ref_file:mtime "$("${WD}/stat_mtime" "$target")
" "${WD}/stat_mtime" "$tempfile"

t mtime:setup "-d 2007-11-12T10:15:30Z $tempfile"
atime=$("${WD}/stat_atime" "$tempfile")
t mtime "-m $tempfile"
t_cmd mtime:atime "$atime
" "${WD}/stat_atime" "$tempfile"

t ref_mtime:setup "-d 2007-11-12T10:15:30Z $tempfile"
atime=$("${WD}/stat_atime" "$tempfile")
t ref_mtime "-m -r $target $tempfile"
t_cmd ref_mtime:atime "$atime
" "${WD}/stat_atime" "$tempfile"
t_cmd ref_mtime:mtime "$("${WD}/stat_mtime" "$target")
" "${WD}/stat_mtime" "$tempfile"

t atime:setup "-d 2007-11-12T10:15:30Z $tempfile"
mtime=$("${WD}/stat_mtime" "$tempfile")
t atime "-a $tempfile"
t_cmd atime:mtime "$mtime
" "${WD}/stat_mtime" "$tempfile"

t ref_atime:setup "-d 2007-11-12T10:15:30Z $tempfile"
mtime=$("${WD}/stat_mtime" "$tempfile")
t ref_atime "-a -r $target $tempfile"
t_cmd ref_atime:mtime "$mtime
" "${WD}/stat_mtime" "$tempfile"
t_cmd ref_atime:atime "$("${WD}/stat_atime" "$target")
" "${WD}/stat_atime" "$tempfile"

t amtime "-a -m $tempfile"

t ref_amtime:setup "-d 2007-11-12T10:15:30Z $tempfile"
t ref_amtime "-a -m -r $target $tempfile"
t_cmd ref_amtime:atime "$("${WD}/stat_atime" "$target")
" "${WD}/stat_atime" "$tempfile"
t_cmd ref_amtime:mtime "$("${WD}/stat_mtime" "$target")
" "${WD}/stat_mtime" "$tempfile"

t dir "${tempdir}"

t dir_optd "-d 2003-06-02T13:37:42Z $tempdir"

TZ=UTC t dir_optd_tz:utc "-d 2003-06-02T13:37:42Z $tempdir"
TZ=UTC t dir_optd_tz:0666 "-d 2003-06-02T13:37:42+0666 $tempdir"

t optt "-t 200306021337.42 $tempfile"

touch_empty_str() { "$target" ''; }
t_cmd --exit=1 empty_str "touch: error: Failed opening file '': No such file or directory
" touch_empty_str

t_cmd rm_tempfile '' rm "${tempfile}"
t_cmd rm_tempdir '' rm -r "${tempdir}"
