#!/usr/bin/env cram
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

  $ export PATH="$TESTDIR/../cmd:$PATH"

  $ test "$(command -v rm)" = "$TESTDIR/../cmd/rm"

  $ touch file
  $ rm file
  $ test ! -e file

POSIX rm(1p) step 1a, no -f option
  $ test ! -f enoent
  $ touch exists
  $ test -f exists
  $ rm enoent exists
  rm: error: Failed getting status for 'enoent': No such file or directory
  [1]
  $ test ! -e exists

POSIX rm(1p) step 1a, -f option
Note: Still printing an error message, even if POSIX doesn't requires it
  $ touch exists_f
  $ test -f exists_f
  $ test ! -f enoent
  $ rm -f enoent exists_f
  $ test ! -e exists_f

POSIX rm(1p) step 2a:
  $ mkdir no_rR.d
  $ test -d no_rR.d
  $ touch no_rR.f
  $ test -f no_rR.f
  $ rm no_rR.d no_rR.f
  rm: error: Is a directory, pass -r or -d to remove: no_rR.d
  [1]
  $ test -d no_rR.d
  $ test ! -e no_rR.f
  $ rm -r no_rR.d
  $ test ! -e no_rR.d

POSIX rm(1p) step 2b, empty directory, no -i
  $ mkdir 2b-empty-noi
  $ test -d 2b-empty-noi
  $ rm -r 2b-empty-noi
  $ test ! -e 2b-empty-noi

POSIX rm(1p) step 2b, empty directory, -i
Note: Ignoring if standard input is a terminal
  $ mkdir 2b-empty-no
  $ test -d 2b-empty-no
  $ printf '%s\n' | rm -ri 2b-empty-no
  rm: Recurse into '2b-empty-no' ? [y/N] 
  rm: Got empty response, considering it false
  $ test -e 2b-empty-no
  $ printf '%s\n' y n | rm -ri 2b-empty-no
  rm: Recurse into '2b-empty-no' ? [y/N] y
  rm: Remove '2b-empty-no' ? [y/N] n
  $ test -e 2b-empty-no
  $ printf '%s\n' y y | rm -ri 2b-empty-no
  rm: Recurse into '2b-empty-no' ? [y/N] y
  rm: Remove '2b-empty-no' ? [y/N] y
  $ test ! -e 2b-empty-no

POSIX rm(1p) step 2c, don't follow symlinks
  $ mkdir 2c-origin
  $ mkdir 2c-symlinks
  $ ln -s 2c-origin 2c-symlinks/dir
  $ touch 2c-origin/file
  $ ln -s 2c-origin/file 2c-symlinks/file
  $ test -L 2c-symlinks/file
  $ test -L 2c-symlinks/dir
  $ rm -r 2c-symlinks
  $ test ! -e 2c-symlinks
  $ test -d 2c-origin
  $ test -f 2c-origin/file
  $ rm -r 2c-origin

POSIX rm(1p) step 3, no write
Extra check from rm(1), unrelated to the EPERM that unlink gets on a non-writable directory
  $ touch no_write
  $ chmod -- -w no_write
  $ echo | rm no_write
  rm: Remove non-writable 'no_write' ? [y/N] 
  rm: Got empty response, considering it false
  $ test -f no_write
  $ echo n | rm no_write
  rm: Remove non-writable 'no_write' ? [y/N] n
  $ test -f no_write
  $ echo y | rm no_write
  rm: Remove non-writable 'no_write' ? [y/N] y
  $ test ! -f no_write




  $ touch file_i
  $ echo | rm -i file_i
  rm: Remove 'file_i' ? [y/N] 
  rm: Got empty response, considering it false
  $ test -e file_i
  $ rm file_i

  $ touch file_in
  $ echo n | rm -i file_in
  rm: Remove 'file_in' ? [y/N] n
  $ test -e file_in
  $ rm file_in

  $ touch file_iy
  $ echo y | rm -i file_iy
  rm: Remove 'file_iy' ? [y/N] y
  $ test ! -e file_iy

Verbose
  $ touch file_v
  $ rm -v file_v
  rm: Removed: file_v


POSIX, -f shouldn't return an error when non-existent files are passed
  $ rm -f /var/empty/e/no/ent
  $ rm /var/empty/e/no/ent
  rm: error: Failed getting status for '/var/empty/e/no/ent': No such file or directory
  [1]

POSIX, -f shouldn't return an error when no operands are passed
  $ rm -f
  $ rm
  rm: error: missing operand
  Usage: rm [-firRv] [files ...]
  [1]

POSIX 2024/D4.1, -d
  $ mkdir empty_dir
  $ rm -d empty_dir
  $ test ! -e empty_dir
  $ mkdir non_empty_dir
  $ touch non_empty_dir/.keep
  $ rm -d non_empty_dir
  rm: error: Couldn't remove 'non_empty_dir': Directory not empty
  [1]
  $ test -e non_empty_dir
  $ rm -fr non_empty_dir

Don't follow symlinks
  $ touch bar
  $ ln bar foo
  $ rm foo
  $ test -e bar
  $ test ! -e foo
  $ rm bar

Correct path when multiple files are given
  $ touch path_v_foo path_v_bar
  $ rm -v path_v_foo path_v_enoent path_v_bar
  rm: Removed: path_v_foo
  rm: error: Failed getting status for 'path_v_enoent': No such file or directory
  rm: Removed: path_v_bar
  [1]
  $ test ! -e path_v_foo
  $ test ! -e path_v_bar
  $ mkdir -p dir_v_foo/ dir_v_bar/ dir_v_dir/foo.d
  $ touch dir_v_foo/bar dir_v_foo/baz dir_v_bar/foo
  $ rm -rv dir_v_foo dir_v_foo/baz dir_v_bar/foo dir_v_bar dir_v_dir/foo.d dir_v_dir
  rm: Removed: dir_v_foo/ba[zr] (re)
  rm: Removed: dir_v_foo/ba[zr] (re)
  rm: Removed: dir_v_foo
  rm: error: Failed getting status for 'dir_v_foo/baz': No such file or directory
  rm: Removed: dir_v_bar/foo
  rm: Removed: dir_v_bar
  rm: Removed: dir_v_dir/foo.d
  rm: Removed: dir_v_dir
  [1]
  $ test ! -e dir_v_foo
  $ test ! -e dir_v_bar
  $ test ! -e dir_v_dir

No files should be left
  $ find .
  .
