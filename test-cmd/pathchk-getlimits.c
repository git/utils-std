// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 202405L
#include <stdio.h>
#include <limits.h>

int main(void)
{
	(void)printf("NAME_MAX=%zd\nPATH_MAX=%zd\n", NAME_MAX, PATH_MAX);
	return 0;
}
