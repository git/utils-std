#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
target="${WD}/cmd/paste"
plans=3
. "${WD}/test-cmd/tap.sh"
. "${WD}/test-cmd/init_env.sh"

banzai=$(mktemp)

seq 1 1000 > "$banzai"
cut -b 1-500 -n "$banzai" > "${banzai}.1"
cut -b 501-  -n "$banzai" > "${banzai}.2"

t_file concat "$banzai" -d "\0" "${banzai}.1" "${banzai}.2"

rm "$banzai" "${banzai}.1" "${banzai}.2"

t_foobar_z() {
	printf '%s\0' foo bar baz | "$target" -z - -
}
t_cmd foobar_z "$(printf 'foo\tbar\0baz\t\0')" t_foobar_z

t --input='foo bar
baz' input_noargs '' 'foo bar
baz
'
