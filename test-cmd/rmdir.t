#!/usr/bin/env cram
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

  $ export PATH="$TESTDIR/../cmd:$PATH"

  $ test "$(command -v rmdir)" = "$TESTDIR/../cmd/rmdir"

  $ mkdir -p no_p/bar
  $ rmdir no_p/bar
  $ test -d no_p
  $ rm -r no_p

  $ mkdir -p p/bar
  $ rmdir -p p/bar
  $ test ! -e p

  $ mkdir -p v_no_p/bar
  $ rmdir -v v_no_p/bar
  rmdir: Removed 'v_no_p/bar'
  $ test -d v_no_p
  $ rm -r v_no_p

  $ mkdir -p v_p/bar
  $ rmdir -pv v_p/bar
  rmdir: Removed 'v_p/bar'
  rmdir: Removed 'v_p'
  $ test ! -e v_p

  $ touch file
  $ rmdir file
  rmdir: error: Failed removing 'file': Not a directory
  [1]
  $ rmdir -p file
  rmdir: error: Failed removing 'file': Not a directory
  [1]
  $ rmdir -v file
  rmdir: error: Failed removing 'file': Not a directory
  [1]
  $ rmdir -pv file
  rmdir: error: Failed removing 'file': Not a directory
  [1]
  $ rm file

  $ mkdir -p e_not_empty/a/b/c/d empty/a/b/c
  $ rmdir -p e_not_empty/a/b/c empty/a/b/c
  rmdir: error: Failed removing 'e_not_empty/a/b/c': Directory not empty
  [1]
  $ test -d e_not_empty/a/b/c
  $ test ! -e empty
  $ rm -r e_not_empty

  $ find .
  .
