#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/uname"

has_cmd ()
{
	command -v "$@" >/dev/null
}

plans=5

. "$(dirname "$0")/tap.sh"

t 'no args' '' "$(uname -s)
"
t 's' '-s' "$(uname -s)
"
t 'a' '-a' "$(uname -s) $(uname -n) $(uname -r) $(uname -v) $(uname -m)
"
if has_cmd hostname; then
	t 'n' '-n' "$(hostname)
"
else
	skip n 'n' "no hostname command"
fi
if has_cmd arch; then
	t 'm' '-m' "$(arch)
"
else
	skip 'm' "no arch command"
fi
