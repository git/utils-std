#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/shuf"
plans=5
. "$(dirname "$0")/tap.sh"

t --input='' noargs '' ''

t --input='foo' stdin '' 'foo'
t --input='foo' stdin_dash '-' 'foo'

# ioctl fail on BSDs
# t devnull '/dev/null' ''

t_seq_shuf_wc() {
	seq $seq | "$target" "$@" | wc -l | tr -d '[:space:]'
}

seq=20
t_cmd seq20_n10 '10' t_seq_shuf_wc -n 10

seq=7
t_cmd seq7_n10 '7' t_seq_shuf_wc -n 10
