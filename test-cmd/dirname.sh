#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/dirname"
plans=10
. "$(dirname "$0")/tap.sh"

# atf_check -e "inline:usage: dirname string\n" -s exit:1 ../cmd/dirname

t '/usr/bin' '/usr/bin' '/usr
'
t '/usr//bin' '/usr//bin' '/usr
'
t '-- /usr//bin' '/usr//bin' '/usr
'
t '.' '.' '.
'
t '-' '-' '.
'
t '/' '/' '/
'
t '///' '///' '/
'
t '---' '---' '.
'
t '--foo' '--foo' '.
'
t './foobar' './foobar' '.
'

# atf_check -s exit:1 -e "inline:usage: dirname string\n" ../cmd/dirname -a "/usr//bin"
