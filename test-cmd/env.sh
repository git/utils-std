#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
target="${WD}/cmd/env"

. "${WD}/test_functions.sh"

plans=10
gentoo_sandbox && plans=6
. "${WD}/test-cmd/tap.sh"

t exec_true 'true' ''
t exec_echo 'echo' '
'
t --exit=1 exec_false 'false' ''
t --exit=127 enoent '/var/empty/e/no/ent' "env: error: Failed executing '/var/empty/e/no/ent': No such file or directory
"

t posix_me_harder "-i FOO=BAR sh -c echo" '
'

if grep -q HAS_WORDEXP "${WD}/config.h"; then
	t_args opt_S 'foo
bar
baz
' "-S printf '%s\n' foo bar baz"
else
	skip opt_S 'Lacks <wordexp.h>'
fi

if ! gentoo_sandbox
then
	t reset '-i FOO=BAR' 'FOO=BAR
'
	t reset_chain "-i FOO=BAR $target" 'FOO=BAR
'
	t uflag "-i FOOZ=BARZ $target -u FOOZ FOO=BAR" 'FOO=BAR
'
	if grep -q HAS_GETOPT_LONG "${WD}/config.h"; then
		t unsetflag "-i FOOZ=BARZ $target --unset=FOOZ FOO=BAR" 'FOO=BAR
'
	else
		skip unsetflag 'Lacks getopt_long'
	fi
	# atf_check -s not-exit:0 -e "inline:env: Error: Unrecognised option: '-f'\nenv [-i] [-u key | --unset=key] [key=value ...] [command [args]]\n" ../cmd/env -f
fi
