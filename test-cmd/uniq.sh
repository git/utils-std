#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/uniq"
plans=7
. "$(dirname "$0")/tap.sh"

t --input='' noargs '' ''

foo_nl='foo
'
t --input="$foo_nl" stdin '' "$foo_nl"

t --input="$foo_nl" stdin_dash '-' "$foo_nl"

t_ltrim() {
	"$target" "$@" | sed 's;[[:space:]]*;;'
}

t_cmd posix_cf1 \
'1 #01 foo0 bar0 foo1 bar1
1 #02 bar0 foo1 bar1 foo1
1 #03 foo0 bar0 foo1 bar1
1 #04
2 #05 foo0 bar0 foo1 bar1
1 #07 bar0 foo1 bar1 foo0
' t_ltrim -c -f 1 "${WD}/uniq_0I.txt"

t_args posix_df1 '#05 foo0 bar0 foo1 bar1
' -d -f 1 "${WD}/uniq_0I.txt"

t_args posix_uf1 \
'#01 foo0 bar0 foo1 bar1
#02 bar0 foo1 bar1 foo1
#03 foo0 bar0 foo1 bar1
#04
#07 bar0 foo1 bar1 foo0
' -u -f 1 "${WD}/uniq_0I.txt"

t_args posix_ds2 '' -d -s 2 "${WD}/uniq_0I.txt"
