#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="$(realpath "$WD/../cmd/pwd")"
plans=6
. "$(dirname "$0")/tap.sh"

t noargs '' "${PWD?}
"

t --exit=1 usage '-H' "pwd: error: Unrecognised option '-H'
Usage: pwd [-L|-P]
"

oldpwd=$PWD

PWD=/$PWD t 'PWD=/$PWD' '' "/${oldpwd}
"

PWD=//foo t 'PWD=//foo' '' "${oldpwd}
"

PWD=foo t PWD=foo '' "${oldpwd}
"

PWD=$oldpwd

if command -v mktemp >/dev/null 2>/dev/null
then
	tmpdir="$(mktemp -d)" || exit 1
	test -d "$tmpdir" || exit 1
	oldpwd="$PWD"
	cd "$tmpdir" || exit 1
	(
		cd "$oldpwd" || exit 1
		rm -r "$tmpdir" || exit 1
	)
	t --exit=1 enoent '' 'pwd: error: getcwd: No such file or directory
'
	cd "$oldpwd"
else
	skip enoent 'requires mktemp(1)'
fi
