#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
target="${WD}/cmd/nice"
getprio="${WD}/test-cmd/getpriority"
plans=4

if ! test -f "${getprio}"; then
	echo '1..0 # SKIP: missing executable: ' "${getprio}"
	exit 0
fi

curr_nice=$("${getprio}")
if [ "$curr_nice" != "getpriority: 0" ]; then
	echo '1..0 # SKIP: need 0 nice value, got: ' "${curr_nice}"
	exit 0
fi

. "${WD}/test-cmd/tap.sh"

t noargs ''

t_args noadj 'getpriority: 0
' "$getprio"

t_args adj:0 'getpriority: 0
' -n 0 "$getprio"

t_args adj:10 'getpriority: 10
' -n 10 "$getprio"
