#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/base64"
plans=21
. "$(dirname "$0")/tap.sh"

t devnull '/dev/null' ''

# Test vectors from RFC4648
t --input='' 'rfc4648:' '' ''
t --input='f' 'rfc4648:f' '' 'Zg==
'
t --input='fo' 'rfc4648:fo' '' 'Zm8=
'
t --input='foo' 'rfc4648:foo' '' 'Zm9v
'
t --input='foob' 'rfc4648:foob' '' 'Zm9vYg==
'
t --input='fooba' 'rfc4648:fooba' '' 'Zm9vYmE=
'
t --input='foobar' 'rfc4648:foobar' '' 'Zm9vYmFy
'

t --input='' 'rfc4648_decode:' '' ''
t --input='Zg==\n' 'rfc4648_decode:Zg==\n' '-d' 'f'
t --input='Zm8=\n' 'rfc4648_decode:Zm8=\n' '-d' 'fo'
t --input='Zm9v\n' 'rfc4648_decode:Zm9v\n' '-d' 'foo'
t --input='Zm9vYg==\n' 'rfc4648_decode:Zm9vYg==\n' '-d' 'foob'
t --input='Zm9vYmE=\n' 'rfc4648_decode:Zm9vYmE=\n' '-d' 'fooba'
t --input='Zm9vYmFy\n' 'rfc4648_decode:Zm9vYmFy\n' '-d' 'foobar'

t_file multiliner_encode "$WD/outputs/base64/all_bytes_w76" "$WD/inputs/all_bytes"

t_file multiliner_decode "$WD/inputs/all_bytes" -d "$WD/outputs/base64/all_bytes_w76"

t_file oneliner_encode "$WD/outputs/base64/all_bytes_w0" -w0 "$WD/inputs/all_bytes"

t_file oneliner_decode "$WD/inputs/all_bytes" -d -w0 "$WD/outputs/base64/all_bytes_w0"

if [ "$(id -u)" = 0 ]
then
	skip noperm 'needs to be run unprivileged'
else
	touch "$WD/inputs/chmod_000"
	chmod 0000 "$WD/inputs/chmod_000"
	t --exit=1 'noperm' "$WD/inputs/chmod_000" "base64: error: Failed opening file '$WD/inputs/chmod_000': Permission denied
"
	chmod 0600 "$WD/inputs/chmod_000"
	rm "$WD/inputs/chmod_000"
fi

#atf_test_case devfull
#devfull_body() {
#	error='inline:base64: Error writing: No space left on device\n'
#	[ "$(uname -s)" = "NetBSD" ] && error='inline:base64: Error writing: Inappropriate ioctl for device\n'
#	[ "$(uname -s)" = "FreeBSD" ] && error='inline:base64: Error writing: Inappropriate ioctl for device\n'
#
#	atf_check -s exit:1 -e "$error" sh -c '../cmd/base64 inputs/all_bytes >/dev/full'
#	atf_check -s exit:1 -e "$error" sh -c '../cmd/base64 <inputs/all_bytes >/dev/full'
#	atf_check -s exit:1 -e "$error" sh -c '../cmd/base64 - <inputs/all_bytes >/dev/full'
#}

#t --exit=1 'readslash' '/' 'base64: Error reading ‘/’: Is a directory
#'

t --exit=1 'enoent' '/var/empty/e/no/ent' "base64: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"
