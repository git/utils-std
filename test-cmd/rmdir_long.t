#!/usr/bin/env cram
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

  $ export PATH="$TESTDIR/../cmd:$PATH"

  $ test "$(command -v rmdir)" = "$TESTDIR/../cmd/rmdir"

  $ grep -q HAS_GETOPT_LONG "${TESTDIR}/../config.h" || return 80

  $ mkdir -p e_not_empty-ign/a/b/c/d empty-ign/a/b/c
  $ rmdir -p --ignore-fail-on-non-empty e_not_empty-ign/a/b/c empty-ign/a/b/c
  $ test -d e_not_empty-ign/a/b/c
  $ test ! -e empty-ign
  $ rm -r e_not_empty-ign

  $ find .
  .
