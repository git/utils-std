#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
target="${WD}/cmd/head"
plans=15
. "${WD}/test-cmd/tap.sh"

t --input="$(seq 1 20)" 20l '' "$(seq 1 10)
"

t --input="$(seq 1 20)" 20l5l '-n 5' "$(seq 1 5)
"

t --input="$(seq 1 20)" 20l-5l '-5' "$(seq 1 5)
"

t --input="$(seq 1 5)" 5l '' "$(seq 1 5)"

t --input="$(seq 1 5)" 5l40l '-n 40' "$(seq 1 5)"

t --input='' empty '' ''
t --input='' empty3l '-n 3' ''
t --input='' empty3c '-c 3' ''

t --input="$(seq 1 2)" devnull+2l "- ${WD}/test-cmd/inputs/empty" "==> - <==
1
2
==> ${WD}/test-cmd/inputs/empty <==
"

t --input="$(seq 1 2)" opt_q "-q - ${WD}/test-cmd/inputs/empty" '1
2'

t --input='' opt_v '-v -' '==> - <==
'

t z3 "-n 3 -z ${WD}/test-cmd/inputs/strings/length" "$(printf '1\0_2\0__3\0')"

set -o pipefail

head_40c() { "$target" -c 40 /dev/zero | wc -c | tr -d '[:space:]'; }
t_cmd 40c '40' head_40c

head_40kc() { "$target" -c 40k /dev/zero | wc -c | tr -d '[:space:]'; }
t_cmd 40c '40960' head_40kc

head_40d() { "$target" -c 40d /dev/zero | wc -c | tr -d '[:space:]'; }
t_cmd --exit=1 40d "head: error: Unrecognised unit 'd'
0" head_40d
