#!/usr/bin/env cram
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

  $ export PATH="$TESTDIR/../cmd:$PATH"

  $ test "$(command -v chown)" = "$TESTDIR/../cmd/chown"

  $ test ! -f enoent
  $ chown "nobody:$(id -Gn nobody)" enoent
  chown: error: Failed getting status for 'enoent': No such file or directory
  [1]

  $ touch id_unchanged
  $ chown -v $(ls -ld id_unchanged | awk '{print $3}') id_unchanged
  chown: Ownership already set to \d+:\d+ for 'id_unchanged' (re)
  $ rm id_unchanged

  $ touch uid_unchanged
  $ chown -v $(ls -lnd uid_unchanged | awk '{print $3}') uid_unchanged
  chown: Ownership already set to \d+:\d+ for 'uid_unchanged' (re)
  $ rm uid_unchanged

  $ touch name_unchanged
  $ chown -v $(ls -ld name_unchanged | awk '{print $3":"$4}') name_unchanged
  chown: Ownership already set to \d+:\d+ for 'name_unchanged' (re)
  $ rm name_unchanged

  $ touch uname_unchanged
  $ chown -v $(ls -ld uname_unchanged | awk '{print $3}') uname_unchanged
  chown: Ownership already set to \d+:\d+ for 'uname_unchanged' (re)
  $ rm uname_unchanged

  $ mkdir loop.d
  $ ln -s ../loop.d/ loop.d/loop
  $ chown -v $(ls -lnd loop.d | awk '{print $3}') loop.d
  chown: Ownership already set to \d+:\d+ for 'loop.d' (re)
  $ chown -v $(ls -lnd loop.d/loop | awk '{print $3}') loop.d/loop
  chown: Ownership already set to \d+:\d+ for 'loop.d/loop' (re)
  $ chown -v -h $(ls -lnd loop.d/loop | awk '{print $3}') loop.d/loop
  chown: Ownership already set to \d+:\d+ for 'loop.d/loop' (re)
  $ chown -v -R $(ls -lnd loop.d/ | awk '{print $3}') loop.d
  chown: Ownership already set to \d+:\d+ for 'loop.d' (re)
  chown: Ownership already set to \d+:\d+ for 'loop.d/loop' (re)
  $ chown -v -R $(ls -lnd loop.d/loop | awk '{print $3}') loop.d/loop
  chown: Ownership already set to \d+:\d+ for 'loop.d/loop' (re)
  $ chown -v -R -P $(ls -lnd loop.d/loop | awk '{print $3}') loop.d/loop
  chown: Ownership already set to \d+:\d+ for 'loop.d/loop' (re)
  $ chown -v -R -H $(ls -lnd loop.d/loop | awk '{print $3}') loop.d/loop
  chown: Ownership already set to \d+:\d+ for 'loop.d/loop' (re)
  chown: Ownership already set to \d+:\d+ for 'loop.d/loop/loop' (re)
 Non-portable, meanwhile adding a depth limit on chown itself could be rather limiting…
 $ chown -v -R -L $(ls -lnd loop.d/loop | awk '{print $3}') loop.d/loop >/dev/null 2>/dev/null
 [1]
  $ rm -r loop.d

  $ touch grp_unchanged
  $ chown -v $(ls -ld grp_unchanged | awk '{print ":"$4}') grp_unchanged
  chown: Ownership already set to \d+:\d+ for 'grp_unchanged' (re)
  $ rm grp_unchanged

  $ find .
  .
