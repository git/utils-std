#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
WD="$(realpath "$WD")"
target="$WD/cmd/split"

t()
{
	count=$((count+1))
	name="$1"

	shift

	if "$@"; then
		printf 'ok %s - %s\n' "$count" "$name"
	else
		printf '# Command failed: %s\n' "$*"
		printf 'not ok %s - %s\n' "$count" "$name"
		err=1
	fi
}

t_end()
{
	rm split_test_*

	cd "${oldpwd?}"
	rm -r "${tempdir?}"

	if [ $count -ne $plans ]
	then
		printf 'error: Ran %d instead of the planned %d tests\n' "$count" "$plans" >&2
		err=1
	fi

	exit $err
}

check_splits_b32()
{
	for i in split_test_b32_*
	do
		set -- $(wc -c "$i")
		if [ "$1" != "32" ]
		then
			printf "# Expected 32 bytes but '%s' is %s bytes\n" "$i" "$size"
			return 1
		fi
	done
}

check_splits_l10()
{
	for i in split_test_l10_*
	do
		set -- $(wc -l "$i")
		if [ "$1" != "10" ]
		then
			printf "# Expected 10 lines but '%s' has %s lines\n" "$i" "$size"
			return 1
		fi
	done
}

err=0
plans=6
count=0

printf '1..%d\n' "$plans"

oldpwd="$PWD"
tempdir="$(mktemp -d)"
cd "$tempdir" || exit 1

trap t_end EXIT

###

t split_b32 "$target" -b 32 "$WD/test-cmd/inputs/all_bytes" split_test_b32_

t check_splits check_splits_b32

cat split_test_b32_* > split_test_b32
t compare cmp split_test_b32 "$WD/test-cmd/inputs/all_bytes"

###

seq 1 100 > test_100

t split_l10 "$target" -l 10 test_100 split_test_l10_

t check_splits check_splits_l10

cat split_test_l10_* > split_test_l10
t compare cmp split_test_l10 test_100
