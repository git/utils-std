#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/readlink"
plans=12
. "$(dirname "$0")/tap.sh"

t --exit=1 noargs '' 'readlink: error: Expected one file as argument, got 0
Usage: readlink [-f|-e] [-n|-z] file...
'

t_cmd create_foobar '' ln -s //example.org/ foobar

t read_foobar foobar '//example.org/
'

t read_foobar_no_nl '-n foobar' '//example.org/'

t_cmd rm:foobar '' rm foobar

t --exit=1 enoent /var/empty/e/no/ent "readlink: error: Failed reading symbolic link of '/var/empty/e/no/ent': No such file or directory
"

t_cmd touch:regular '' touch regular
t --exit=1 readlink:regular regular "readlink: error: Failed reading symbolic link of 'regular': Invalid argument
"
t_cmd rm:regular '' rm regular

t_cmd mkdir:dir '' mkdir dir
t --exit=1 readlink:dir dir "readlink: error: Failed reading symbolic link of 'dir': Invalid argument
"
t_cmd rm:dir '' rm -r dir
