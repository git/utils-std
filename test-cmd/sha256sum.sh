#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/sha256sum"
plans=16
. "$(dirname "$0")/tap.sh"

if test "$(uname -s)" = "FreeBSD"
then
	skip devnull 'FreeBSD treats posix_fadvise on /dev/null as invalid'
else
	t devnull '/dev/null' 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855  /dev/null
'
fi
t empty "$WD/inputs/empty" "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855  $WD/inputs/empty
"
t --input='' 'empty_stdin' '' 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
'

t --input='abc' 'abc' '' 'ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad
'

t --exit=1 'enoent' '/var/empty/e/no/ent' "sha256sum: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"

t --input="e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855  $WD/inputs/empty" 'check:empty' '-c' "$WD/inputs/empty: OK
"
t --input="e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 *$WD/inputs/empty" 'bin_check:empty' '-c' "$WD/inputs/empty: OK
"
t --input="e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 *$WD/inputs/empty" 'bin_check:empty:dash' '-c -' "$WD/inputs/empty: OK
"

t --exit=1 --input="ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad  $WD/inputs/empty" 'xfail_check:empty' '-c' "$WD/inputs/empty: FAILED
"
t --exit=1 --input="ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad *$WD/inputs/empty" 'xfail_bin_check:empty' '-c' "$WD/inputs/empty: FAILED
"

t --exit=1 --input='e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855  /var/empty/e/no/ent' 'xfail_check:enoent' '-c' "sha256sum: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"
t --exit=1 --input='e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 */var/empty/e/no/ent' 'xfail_bin_check:enoent' '-c' "sha256sum: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"

t --exit=1 --input="# $WD/inputs/empty" 'invalid_chars:#' '-c' "sha256sum: error: Invalid character '#' while reading hash in line: # $WD/inputs/empty
"
t --exit=1 --input="e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855" 'missing_file' '-c' "sha256sum: error: Failed opening file '': No such file or directory
"
t --exit=1 --input="e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b85  $WD/inputs/empty" 'truncated_hash' '-c' 'sha256sum: error: Got 64 hexadecimal digits while expected 65 for a SHA256
'
t --exit=1 --input="e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855d  $WD/inputs/empty" 'elongated_hash' '-c' 'sha256sum: error: Got 66 hexadecimal digits while expected 65 for a SHA256
'
