#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/cksum"
plans=5
. "$(dirname "$0")/tap.sh"

if test "$(uname -s)" = "FreeBSD"
then
	skip devnull 'FreeBSD treats posix_fadvise on /dev/null as invalid'
else
	t devnull '/dev/null' '4294967295 0 /dev/null
'
fi
t empty "$WD/inputs/empty" "4294967295 0 $WD/inputs/empty
"
t --input='' 'empty_stdin' '' '4294967295 0
'

t all_bytes "$WD/inputs/all_bytes" "1313719201 256 $WD/inputs/all_bytes
"

t --exit=1 'enoent' '/var/empty/e/no/ent' "cksum: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"
