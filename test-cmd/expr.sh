#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
target="${WD}/cmd/expr"
plans=41

. "${WD}/test-cmd/tap.sh"

t_expr_no() {
	name="$1"; shift
	t_args --exit=1 "$name" '0
' "$@"
}
t_expr_ok() {
	name="$1"; shift
	t_args "$name" '1
' "$@"
}

t int 1 '1
'

t_args group '4
' '(' 2 '*' 3 ')' - 2

t_args 'char_count:1234' '4
' "X1234" : '.*' - 1
t_args 'char_count:12345' '5
' "X12345" : '.*' - 1

t_expr_no or:00 0 '|' 0
t_expr_ok or:01 0 '|' 1
t_expr_ok or:10 1 '|' 0
t_expr_ok or:11 1 '|' 1

t_expr_no and:00 0 '&' 0
t_expr_no and:01 0 '&' 1
t_expr_no and:10 1 '&' 0
t_expr_ok and:11 1 '&' 1

t_expr_ok eq:00 0 '=' 0
t_expr_no eq:01 0 '=' 1
t_expr_no eq:10 1 '=' 0
t_expr_ok eq:11 1 '=' 1

t_expr_no gt:00 0 '>' 0
t_expr_no gt:01 0 '>' 1
t_expr_ok gt:10 1 '>' 0
t_expr_no gt:11 1 '>' 1

t_expr_no lt:00 0 '<' 0
t_expr_ok lt:01 0 '<' 1
t_expr_no lt:10 1 '<' 0
t_expr_no lt:11 1 '<' 1

t_expr_ok ge:00 0 '>=' 0
t_expr_no ge:01 0 '>=' 1
t_expr_ok ge:10 1 '>=' 0
t_expr_ok ge:11 1 '>=' 1

t_expr_ok le:00 0 '<=' 0
t_expr_ok le:01 0 '<=' 1
t_expr_no le:10 1 '<=' 0
t_expr_ok le:11 1 '<=' 1

t_expr_no ne:00 0 '!=' 0
t_expr_ok ne:01 0 '!=' 1
t_expr_ok ne:10 1 '!=' 0
t_expr_no ne:11 1 '!=' 1

t_args mul '6
' 3 '*' 2
t_args div '3
' 7 / 2
t_args rem '1
' 7 % 2
t_args add '3
' 1 + 2
t_args sub '2
' 3 - 1
