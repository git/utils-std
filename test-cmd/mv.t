#!/usr/bin/env cram
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

  $ export PATH="$TESTDIR/../cmd:$PATH"

  $ test "$(command -v mv)" = "$TESTDIR/../cmd/mv"

POSIX, non-directory target with a trailing slash is an error
  $ touch nondir file
  $ mv file nondir/
  mv: error: Failed opening destination directory 'nondir/': Not a directory
  [1]
  $ test -e file
  $ test -e nondir
  $ mv enoent nondir/
  mv: error: Failed opening destination directory 'nondir/': Not a directory
  [1]
  $ rm nondir file

POSIX mv(1) step 1a, no -f option, no -i option
  $ mkdir -m -w 1a_no-f_no-write
  $ touch src
  $ printf '\n' | mv src 1a_no-f_no-write/dest
  mv: error: Failed moving 'src' to '1a_no-f_no-write/dest': Permission denied
  [1]
  $ test -e src
  $ printf 'n\n' | mv src 1a_no-f_no-write/dest
  mv: error: Failed moving 'src' to '1a_no-f_no-write/dest': Permission denied
  [1]
  $ test -e src
  $ printf 'y\n' | mv src 1a_no-f_no-write/dest
  mv: error: Failed moving 'src' to '1a_no-f_no-write/dest': Permission denied
  [1]
  $ test ! -e src
  [1]
  $ rm -fr 1a_no-f_no-write

POSIX mv(1) step 1b, no -f option, -i passed
  $ mkdir 1a_no-f_write
  $ touch src 1a_no-f_write/dest
  $ printf '\n' | mv -i src 1a_no-f_write/dest
  mv: Destination file '1a_no-f_write/dest' already exists, overwrite? [yN] 
  mv: Got empty response, considering it false
  $ test -e src
  $ test -d 1a_no-f_write
  $ printf 'n\n' | mv -i src 1a_no-f_write/dest
  mv: Destination file '1a_no-f_write/dest' already exists, overwrite? [yN] n
  $ test -f src
  $ test -d 1a_no-f_write
  $ printf 'y\n' | mv -i src 1a_no-f_write/dest
  mv: Destination file '1a_no-f_write/dest' already exists, overwrite? [yN] y
  $ test ! -e src
  $ test -d 1a_no-f_write
  $ rm -fr 1a_no-f_write

POSIX mv(1) step 2, same file
  $ touch same-name
  $ mv same-name same-name
  mv: error: Passed to both source and destination: 'same-name'
  [1]
  $ test -e same-name
  $ rm same-name

POSIX mv(1) step 2, same file, hardlink
  $ touch same-src-h
  $ ln same-src-h same-dest-h
  $ mv same-src-h same-src-h
  mv: error: Passed to both source and destination: 'same-src-h'
  [1]
  $ test -e same-src-h
  $ test -e same-dest-h
  $ rm same-src-h same-dest-h

POSIX mv(1) step 2, same file, symlink
  $ touch same-src-s
  $ ln -s same-src-s same-dest-s
  $ mv same-src-s same-src-s
  mv: error: Passed to both source and destination: 'same-src-s'
  [1]
  $ test -e same-src-s
  $ test -e same-dest-s
  $ rm same-src-s same-dest-s

Where destination is an existing directory
  $ mkdir destdir
  $ touch foo
  $ mv foo destdir
  $ test ! -e foo
  $ test -d destdir
  $ test -f destdir/foo
  $ rm -r destdir

  $ mkdir destdir_trail/
  $ touch foo_trail
  $ mv foo_trail destdir_trail/
  $ test ! -e foo_trail
  $ test -d destdir_trail
  $ test -f destdir_trail/foo_trail
  $ rm -r destdir_trail

Where destination is an existing file
  $ touch foo_file destfile
  $ mv foo_file destfile
  $ test ! -e foo_file
  $ test -f destfile
  $ rm destfile

  $ touch foo_file_i destfile_i
  $ printf 'y\n' | mv -i foo_file_i destfile_i
  mv: Destination file 'destfile_i' already exists, overwrite? [yN] y
  $ test ! -e foo_file_i
  $ test -f destfile_i
  $ rm destfile_i

  $ touch foo_file_trail destfile_trail
  $ mv foo_file_trail destfile_trail/
  mv: error: Failed opening destination directory 'destfile_trail/': Not a directory
  [1]
  $ test -f foo_file_trail
  $ test -f destfile_trail
  $ rm foo_file_trail destfile_trail

Verbose (non-standard)
  $ touch foo
  $ mv -v foo bar
  mv: renamed 'foo' -> 'bar'
  $ test ! -e foo
  $ test -e bar
  $ rm bar

Last component used for destination filename
  $ mkdir -p src_last/dir dest_last
  $ touch src_last/dir/file
  $ mv src_last/dir/file dest_last/
  $ test -f dest_last/file
  $ test ! -e dest_last/dir/file
  $ test ! -e src_last/dir/file
  $ mv src_last/dir dest_last
  $ test -d dest_last/dir
  $ test ! -e src_last/dir
  $ test -f dest_last/file
  $ rm -r src_last dest_last

Works with non-resolving source symlinks
  $ ln -s /var/empty/e/no/ent symlink_enoent
  $ test -L symlink_enoent
  $ mv symlink_enoent symlink_enoent.done
  $ test ! -L symlink_enoent
  $ test -L symlink_enoent.done
  $ rm symlink_enoent.done

No files should be left
  $ find .
  .
