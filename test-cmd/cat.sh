#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/cat"
plans=10
. "$(dirname "$0")/tap.sh"

t --input='' noargs '' ''

t --input='foo' stdin '' 'foo'
t --input='foo' stdin_dash '-' 'foo'

t devnull '/dev/null' ''

t_file all_bytes "$WD/inputs/all_bytes" "$WD/inputs/all_bytes"

if [ "$(id -u)" = 0 ]
then
	skip noperm 'needs to be run unprivileged'
else
	touch "$WD/inputs/chmod_000"
	chmod 0000 "$WD/inputs/chmod_000"
	t --exit=1 'noperm' "$WD/inputs/chmod_000" "cat: error: Failed opening file '$WD/inputs/chmod_000': Permission denied
"
	chmod 0600 "$WD/inputs/chmod_000"
	rm "$WD/inputs/chmod_000"
fi

#atf_test_case devfull
#devfull_body() {
#	atf_check -s exit:1 -e 'inline:cat: Error writing: No space left on device\n' sh -c '../cmd/cat inputs/all_bytes >/dev/full'
#	atf_check -s exit:1 -e 'inline:cat: Error writing: No space left on device\n' sh -c '../cmd/cat <inputs/all_bytes >/dev/full'
#	atf_check -s exit:1 -e 'inline:cat: Error writing: No space left on device\n' sh -c '../cmd/cat - <inputs/all_bytes >/dev/full'
#}

if [ "$(uname -s)" = "NetBSD" ]; then
	skip readslash "NetBSD allows to read directories"
else
	t --exit=1 readslash / "cat: error: Failed copying data from file '/': Is a directory
"
fi

t --exit=1 enoent /var/empty/e/no/ent "cat: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"

t_file doubledash "$WD/inputs/all_bytes" -- "$WD/inputs/all_bytes"

t --exit=1 tripledash "--- $WD/inputs/all_bytes" "cat: error: Unrecognised option: '--'
Usage: cat [-u] [files ...]
"
