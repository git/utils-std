#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
target="${WD}/cmd/wc"
plans=24
. "${WD}/test-cmd/tap.sh"
. "${WD}/test-cmd/init_env.sh"

t --input='a b c' 'a b c' '' '0 3 5
'

t --input='a
b
c
' 'a\nb\nc' '' '3 3 6
'

t --input='a
b
c
' 'a\nb\nc' '-clw' '3 3 6
'

t --input='a
bb
ccc
' 'a\nbb\nccc\n' '' '3 3 9
'

t --input='a
bb
ccc
' 'a\nbb\nccc\n:-clw' '-clw' '3 3 9
'


t --input='я нет 草' 'ru+jp:-m' '-m' '7
'
t --input='я нет 草' 'ru+jp:-mw' '-mw' '3 7
'
t --input='я нет 草' 'ru+jp:-mlw' '-mlw' '0 3 7
'

t --input='' 'empty_stdin' '' '0 0 0
'
t 'empty_file' "${WD}/test-cmd/inputs/empty" "0 0 0 ${WD}/test-cmd/inputs/empty
"

t --exit=1 'enoent' '/var/empty/e/no/ent' "wc: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"

# Formatting with reduced counts

t --input='
' '\n:1l' '-l' '1
'

t --input='a
' 'a\n:1l' '-l' '1
'

t --input='
' '\n:1c' '-c' '1
'

t --input='a
' 'a\n:2c' '-c' '2
'

t --input='
' '\n:0w' '-w' '0
'

t --input='a
' 'a\n:1w' '-w' '1
'

t --input='
' '\n:1l1c' '-lc' '1 1
'

t --input='a
' 'a\n:1l2c' '-lc' '1 2
'

t --input='' 'empty_stdin:0c' '-c' '0
'
t --input='' 'empty_stdin:0c0l' '-cl' '0 0
'
t --input='' 'empty_stdin:0w' '-w' '0
'

# Total when multiple files are specified

t --input='foo
' 'foo+empty' "${WD}/test-cmd/inputs/empty -" "\
0 0 0 ${WD}/test-cmd/inputs/empty
1 1 4
1 1 4 total
"

t --input='foo
' 'foo+empty:c' "-c ${WD}/test-cmd/inputs/empty -" "\
0 ${WD}/test-cmd/inputs/empty
4
4 total
"
