#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/logname"
plans=1
. "$(dirname "$0")/tap.sh"

if ! test -n "${LOGNAME}"; then
	skip basic '$LOGNAME not set (wtf)'
else
	t basic '' "${LOGNAME}
"
fi
