#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/realpath"
plans=31
. "$(dirname "$0")/tap.sh"

t . '.' "${PWD}
"

t / / '/
'
t /var /var '/var
'
t /var/empty /var/empty '/var/empty
'
t --exit=1 /var/empty/foo/bar /var/empty/foo/bar 'realpath: error: Failed canonilizing parent of full path "/var/empty/foo/bar": No such file or directory
'
t --exit=1 /var/empty/foo/bar/ /var/empty/foo/bar/ 'realpath: error: Failed canonilizing parent of full path "/var/empty/foo/bar": No such file or directory
'
t --exit=1 /var/empty/foo/bar// /var/empty/foo/bar// 'realpath: error: Failed canonilizing parent of full path "/var/empty/foo/bar": No such file or directory
'

t e:/ '-e /' '/
'
t e:/var '-e /var' '/var
'
t --exit=1 e:/var/empty/foo '-e /var/empty/foo' 'realpath: error: Failed canonilizing "/var/empty/foo": No such file or directory
'
t --exit=1 e:/var/empty/foo/ '-e /var/empty/foo/' 'realpath: error: Failed canonilizing "/var/empty/foo/": No such file or directory
'
t --exit=1 e:/var/empty/foo// '-e /var/empty/foo//' 'realpath: error: Failed canonilizing "/var/empty/foo//": No such file or directory
'
t --exit=1 e:/var/empty/foo/bar '-e /var/empty/foo/bar' 'realpath: error: Failed canonilizing "/var/empty/foo/bar": No such file or directory
'
t --exit=1 e:/var/empty/foo/bar/ '-e /var/empty/foo/bar/' 'realpath: error: Failed canonilizing "/var/empty/foo/bar/": No such file or directory
'
t --exit=1 e:/var/empty/foo/bar// '-e /var/empty/foo/bar//' 'realpath: error: Failed canonilizing "/var/empty/foo/bar//": No such file or directory
'

# Non-directory

t /dev/null /dev/null '/dev/null
'
t e:/dev/null '-e /dev/null' '/dev/null
'
t E:/dev/null '-E /dev/null' '/dev/null
'

t --exit=1 /dev/null/ /dev/null/ 'realpath: error: Failed canonilizing "/dev/null/": Not a directory
'
t --exit=1 e:/dev/null/ '-e /dev/null/' 'realpath: error: Failed canonilizing "/dev/null/": Not a directory
'
t --exit=1 E:/dev/null/ '-E /dev/null/' 'realpath: error: Failed canonilizing "/dev/null/": Not a directory
'

t --exit=1 /dev/null/.. /dev/null/.. 'realpath: error: Failed canonilizing "/dev/null/..": Not a directory
'
t --exit=1 e:/dev/null/.. '-e /dev/null/..' 'realpath: error: Failed canonilizing "/dev/null/..": Not a directory
'
t --exit=1 E:/dev/null/.. '-E /dev/null/..' 'realpath: error: Failed canonilizing "/dev/null/..": Not a directory
'

# Zero-separation

# t z:/dev/null_/var/empty '-z /dev/null /var/empty' $'/dev/null\0/var/empty\0'

# No final newline
t n:/dev/null_/var/empty '-n /dev/null /var/empty' '/dev/null
/var/empty'

# As required by POSIX.1-2024 -E and -e aren't erroneous
t Ee:/dev/null '-E -e /dev/null' '/dev/null
'

t s:foo '-s foo' "$PWD/foo
"
t s:foo/bar '-s foo/bar' "$PWD/foo/bar
"
t s:./foo/bar '-s ./foo/bar' "$PWD/foo/bar
"
t s:/foo/bar '-s /foo/bar' "/foo/bar
"
t s:/foo/del/../bar '-s /foo/del/../bar' "/foo/bar
"
