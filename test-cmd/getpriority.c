// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 202405L
#include <stdio.h>
#include <sys/resource.h>

int main(void)
{
	int ret = getpriority(PRIO_PROCESS, 0);
	if(ret == -1) perror("getpriority: error");

	printf("getpriority: %d\n", ret);

	return 0;
}
