#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/cmp"
plans=8
. "$(dirname "$0")/tap.sh"

printf foo > foo
printf bar > bar

t foofoo 'foo foo'
t barbar 'bar bar'

t --exit=1 foobar 'foo bar' 'foo bar differ: char 1, line 1
'
t --exit=1 barfoo 'bar foo' 'bar foo differ: char 1, line 1
'

t --exit=1 s_foobar '-s foo bar'
t --exit=1 s_barfoo '-s bar foo'

rm foo bar

seq 1 3 > seq_1_3
seq 1 2 > seq_1_2

t --exit=1 seq_1_3-1_2 'seq_1_3 seq_1_2' 'cmp: error: EOF on seq_1_2 line 3
'
t --exit=1 s_seq_1_3-1_2 '-s seq_1_3 seq_1_2'

rm seq_1_3 seq_1_2
