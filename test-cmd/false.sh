#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: 0BSD

target="$(dirname "$0")/../cmd/false"
plans=3
. "$(dirname "$0")/tap.sh"

t --exit=1 basic '' ''
t --exit=1 nohelp '--help' ''
t --exit=1 noversion '--version' ''
