#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/strings"
plans=26
. "$(dirname "$0")/tap.sh"

usage="strings: [-az] [-t format] [-n number] [file...]
"

t --exit=1 usage "-t aa $WD/inputs/all_bytes" "${usage}"

t devnull /dev/null ''

t_file all_bytes "$WD/outputs/strings/all_bytes" "$WD/inputs/all_bytes"

t default "${WD}/inputs/strings/length" '___4
____5
_____6
______7
_______8
________9
'

t n8 "-n 8 ${WD}/inputs/strings/length" '_______8
________9
'

t z8 "-z -n 8 ${WD}/inputs/strings/length" "$(printf '_______8\0________9\0')"

#atf_test_case noperm cleanup
#noperm_body() {
#	touch inputs/chmod_000 || atf_fail "touching chmod_000"
#	chmod 0000 inputs/chmod_000 || atf_fail "chmod 0000 chmod_000"
#	# shellcheck disable=SC1112
#	atf_check -s exit:1 -e 'inline:strings: Error opening ‘inputs/chmod_000’: Permission denied\n' ../cmd/strings inputs/chmod_000
#}
#noperm_cleanup() {
#	chmod 0600 inputs/chmod_000 || atf_fail "chmod 0600 chmod_000"
#	rm inputs/chmod_000 || atf_fail "rm chmod_000"
#}

t octal:devnull '-to /dev/null' ''
t_file octal:all_bytes "$WD/outputs/strings/all_bytes_to" -to "$WD/inputs/all_bytes"
t_file octal:lengthelf "$WD/outputs/strings/length_to" -to "$WD/inputs/strings/length"
t_file octal:length8elf "$WD/outputs/strings/length_8_to" -to -n 8 "$WD/inputs/strings/length"

t hex:devnull '-tx /dev/null' ''
t_file hex:all_bytes "$WD/outputs/strings/all_bytes_tx" -tx "$WD/inputs/all_bytes"
t_file hex:lengthelf "$WD/outputs/strings/length_tx" -tx "$WD/inputs/strings/length"
t_file hex:length8elf "$WD/outputs/strings/length_8_tx" -tx -n 8 "$WD/inputs/strings/length"

t dec:devnull '-td /dev/null' ''
t_file dec:all_bytes "$WD/outputs/strings/all_bytes_td" -td "$WD/inputs/all_bytes"
t_file dec:lengthelf "$WD/outputs/strings/length_td" -td "$WD/inputs/strings/length"
t_file dec:length8elf "$WD/outputs/strings/length_8_td" -td -n 8 "$WD/inputs/strings/length"

fmt_t="strings: error: Unknown format: t
${usage}"
t --exit=1 fmt_t:devnull '-tt /dev/null' "${fmt_t}"
t --exit=1 fmt_t:all_bytes "-tt $WD/inputs/all_bytes" "${fmt_t}"
t --exit=1 fmt_t:lengthelf "-tt $WD/inputs/strings/length" "${fmt_t}"
t --exit=1 fmt_t:length8elf "-tt -n 8 $WD/inputs/strings/length" "${fmt_t}"

t --exit=1 erange_n:0 "-n 0 $WD/inputs/all_bytes" "strings: error: Option \`-n 0\` is too small
${usage}"
t --exit=1 erange_n:4097 "-n 4097 $WD/inputs/all_bytes" "strings: error: Option \`-n 4097\` is too large
${usage}"
t --exit=1 erange_n:f "-n f $WD/inputs/all_bytes" "strings: error: Option \`-n f\`: Invalid argument
${usage}"
t --exit=1 erange_n:42f "-n 42f $WD/inputs/all_bytes" "strings: error: Option \`-n 42f\`: Invalid argument
${usage}"
