#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/sleep"
plans=3
. "$(dirname "$0")/tap.sh"

t --exit=1 noargs '' 'sleep: error: Got a total duration of 0
'

t --exit=1 zero '0' 'sleep: error: Got a total duration of 0
'

t zero-one 0.1 ''
