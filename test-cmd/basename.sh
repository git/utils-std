#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/basename"
plans=9
. "$(dirname "$0")/tap.sh"

t 'no args' '' '.
'
t '/usr/bin' '/usr/bin' 'bin
'
t '/usr//bin' '/usr//bin' 'bin
'
t '-- /usr//bin' '-- /usr//bin' 'bin
'
t 'suffix' '/usr//bin-test -test' 'bin
'
t dash '-' '-
'
t double-dash '--' '.
'
t_args opt_a 'str1
str2
' -a any/str1 any/str2

t_args opt_s 'stdio
unistd
' -s .h include/stdio.h include/unistd.h

#atf_test_case usage
#usage_body() {
#	atf_check -s exit:1 -e "inline:usage: basename string [suffix]\n" ../cmd/basename 1 2 3
#}

#atf_test_case devfull
#devfull_body() {
#	has_glibc && atf_skip "glibc ignoring write errors for puts()"
#	[ "$(uname -s)" = "NetBSD" ] && atf_skip "NetBSD ignoring write errors for puts()"
#	[ "$(uname -s)" = "FreeBSD" ] && atf_skip "FreeBSD ignoring write errors for puts()"
#
#	atf_check -s exit:1 -e 'inline:basename: puts: No space left on device\n' sh -c '../cmd/basename >/dev/full'
#	atf_check -s exit:1 -e 'inline:basename: puts: No space left on device\n' sh -c '../cmd/basename "/usr/bin" >/dev/full'
#	atf_check -s exit:1 -e 'inline:basename: puts: No space left on device\n' sh -c '../cmd/basename "/usr//bin-test" "-test" >/dev/full'
#}
