#!/bin/false
# utils-std: Collection of commonly available Unix tools
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

count=0
err=0

# t [--exit=n] [--input=str] <test_name> <arguments> <expected_output>
t ()
{
	exp_ret=0
	for i; do
		case "$i" in
		--exit=*)
			exp_ret="${i#*=}"
			shift
		;;
		--input=*)
			input="${i#*=}"
			shift
		;;
		--)
			shift
			break
		;;
		# Note: * is still a wildcard, even with a range before
		-[a-zA-Z0-9]|--[a-zA-Z0-9]*=*)
			printf 'Unknown option: %s\n' "$i"
			exit 2
		;;
		*)
			break
		;;
		esac
	done

	if [ "${input+set}" = "set" ]; then
		# Append a final slash so sh(1) doesn't trims final newlines
		out="$(printf "${input?}" | "${target?}" $2 2>&1;r=$?;printf %s /;exit $r)"
		ret="$?"
	else
		# Append a final slash so sh(1) doesn't trims final newlines
		out="$("${target?}" $2 2>&1;r=$?;printf %s /;exit $r)"
		ret="$?"
	fi

	out="${out%/}"
	count=$((count+1))

	if [ "$ret" != "$exp_ret" ]; then
		printf 'not ok %d - %s\n' "$count" "$1"
		printf '# Expected exit code %d, got %d\n' "$exp_ret" "$ret"
		printf "$out" | sed -e 's;^;# ;'
		err=1
	elif [ "$out" != "$3" ]; then
		printf 'not ok %d - %s\n' "$count" "$1"
		printf '# == Expected ==\n'
		echo "$3" | sed -e 's;^;# ;'
		printf '# == Got ==\n'
		echo "$out" | sed -e 's;^;# ;'
		err=1
	else
		printf 'ok %d - %s\n' "$count" "$1"
	fi
}

# t_args [--exit=n] [--input=str] <test_name> <expected_output> <arguments ...>
t_args() {
	exp_ret=0
	for i; do
		case "$i" in
		--exit=*)
			exp_ret="${i#*=}"
			shift
		;;
		--input=*)
			input="${i#*=}"
			shift
		;;
		--)
			shift
			break
		;;
		# Note: * is still a wildcard, even with a range before
		-[a-zA-Z0-9]|--[a-zA-Z0-9]*=*)
			printf 'Unknown option: %s\n' "$i"
			exit 2
		;;
		*)
			break
		;;
		esac
	done
	name="$1"; shift
	exp_out="$1"; shift

	if [ "${input+set}" = "set" ]; then
		# Append a final slash so sh(1) doesn't trims final newlines
		out="$(printf "${input?}" | "${target?}" "$@" 2>&1;r=$?;printf %s /;exit $r)"
		ret="$?"
	else
		# Append a final slash so sh(1) doesn't trims final newlines
		out="$("${target?}" "$@" 2>&1;r=$?;printf %s /;exit $r)"
		ret="$?"
	fi

	out="${out%/}"
	count=$((count+1))

	if [ "$ret" != "$exp_ret" ]; then
		printf 'not ok %d - %s\n' "$count" "$name"
		printf '# Expected exit code %d, got %d\n' "$exp_ret" "$ret"
		printf "$out" | sed -e 's;^;# ;'
		err=1
	elif [ "$out" != "$exp_out" ]; then
		printf 'not ok %d - %s\n' "$count" "$name"
		printf '# == Expected ==\n'
		echo "$exp_out" | sed -e 's;^;# ;'
		printf '# == Got ==\n'
		echo "$out" | sed -e 's;^;# ;'
		err=1
	else
		printf 'ok %d - %s\n' "$count" "$name"
	fi
}

# t_file [--exit=n] [--infile=file] <test_name> <file_expected> [arguments ...]
# file_expected is an existing file to compare output against
t_file()
{
	exp_ret=0
	for i; do
		case "$i" in
		--exit=*)
			exp_ret="${i#*=}"
			shift
		;;
		--infile=*)
			infile="${i#*=}"
			shift
		;;
		--)
			shift
			break
		;;
		# Note: * is still a wildcard, even with a range before
		-[a-zA-Z0-9]|--[a-zA-Z0-9]*=*)
			printf 'Unknown option: %s\n' "$i"
			exit 2
		;;
		*)
			break
		;;
		esac
	done
	name="$1"; shift
	file="$1"; shift

	command -v mktemp >/dev/null 2>/dev/null || \
		skip $1 "t_file requires mktemp(1)"

	count=$((count+1))
	out="$(mktemp)"
	if [ "${infile+set}" = "set" ]; then
		<"$infile" "${target?}" "$@" 2>&1 >"$out"
	else
		"${target?}" "$@" 2>&1 >"$out"
	fi
	ret="$?"

	if [ "$ret" != "$exp_ret" ]; then
		printf 'not ok %d - %s\n' "$count" "$name"
		printf '# Expected exit code %d, got %d\n' "$exp_ret" "$ret"
		printf "$out" | sed -e 's;^;# ;'
		err=1
	elif ! cmp -s "${out?}" "$file"; then
		printf 'not ok %d - %s\n' "$count" "$name"
		diff -u "$out" "$file" | sed -e 's;^;# ;'
		err=1
	else
		printf 'ok %d - %s\n' "$count" "$name"
		rm "$out"
	fi
}

# t_cmd [--exit=n] <test_name> <expected_output> <command> [arguments]
t_cmd() {
	exp_ret=0
	for i; do
		case "$i" in
		--exit=*)
			exp_ret="${i#*=}"
			shift
		;;
		--)
			shift
			break
		;;
		# Note: * is still a wildcard, even with a range before
		-[a-zA-Z0-9]|--[a-zA-Z0-9]*=*)
			printf 'Unknown option: %s\n' "$i"
			exit 2
		;;
		*)
			break
		;;
		esac
	done
	name="$1"; shift
	exp_out="$1"; shift

	# Append a final slash so sh(1) doesn't trims final newlines
	out="$("$@" 2>&1;r=$?;printf %s /;exit $r)"
	ret="$?"
	out="${out%/}"

	count=$((count+1))

	if [ "$ret" != "$exp_ret" ]; then
		printf 'not ok %d - %s\n' "$count" "$name"
		printf '# Expected exit code %d, got %d\n' "$exp_ret" "$ret"
		printf "$out" | sed -e 's;^;# ;'
		err=1
	elif [ "$out" != "$exp_out" ]; then
		printf 'not ok %d - %s\n' "$count" "$name"
		printf '# == Expected ==\n'
		echo "$exp_out" | sed -e 's;^;# ;'
		printf '# == Got ==\n'
		echo "$out" | sed -e 's;^;# ;'
		err=1
	else
		printf 'ok %d - %s\n' "$count" "$name"
	fi
}

t_end ()
{
	if [ $count -ne $plans ]
	then
		printf 'error: Ran %d instead of the planned %d tests\n' "$count" "$plans" >&2
		err=1
	fi

	exit $err
}

# $1 -> name
# $2 -> reason for skipping
skip ()
{
	count=$((count+1))
	name="$1"
	shift
	printf 'ok %s # skip %s\n' "$count $name" "$*"
}

if ! test -f "${target?}"; then
	echo '1..0 # SKIP: missing executable: ' "${target?}"
	exit 0
fi

printf '1..%d\n' "$plans"

trap t_end EXIT
