#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/unlink"
plans=2
. "$(dirname "$0")/tap.sh"

touch unlink-this
t basic 'unlink-this' ''
rm -f unlink-this || exit 1

t --exit=1 enoent /var/empty/e/no/ent "unlink: error: Failed unlinking '/var/empty/e/no/ent': No such file or directory
"
