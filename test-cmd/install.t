#!/usr/bin/env cram
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

  $ export PATH="$TESTDIR/../cmd:$PATH"

# Using chmod -v + to get permission bits, + with no perm nor who doesn't changes mode bits

  $ test "$(command -v install)" = "$TESTDIR/../cmd/install"
  $ test "$(command -v chmod)" = "$TESTDIR/../cmd/chmod"

  $ umask 002

  $ test ! -e src
  $ touch src
  $ install -m 755 src dest
  $ chmod -v 755 dest
  chmod: Permissions already set to 00755/-rwxr-xr-x for 'dest'
  $ chmod -v 755 src
  chmod: Permissions changed from 00664/-rw-rw-r-- to 00755/-rwxr-xr-x for 'src'
  $ rm -f src dest

Integrity check
  $ seq 1 2048 > content
  $ wc -c content
  9133 content
  $ install content content.inst
  $ wc -c content content.inst
  9133 content
  9133 content.inst
  18266 total
  $ cmp content content.inst
  $ rm content content.inst

install -d
  $ test ! -e foo.d
  $ install -d foo.d
  $ test -d foo.d
  $ rm -fr foo.d

  $ test ! -e enoent.d
  $ install -d enoent.d/dir
  $ test -d enoent.d
  $ test -d enoent.d/dir
  $ rm -fr enoent.d

  $ install -d -m 755 dest.d
  $ chmod -v 755 dest.d
  chmod: Permissions already set to 00755/drwxr-xr-x for 'dest.d'
  $ rm -fr dest.d

install -D, single src
  $ test ! -e gramps
  $ touch FooD
  $ install -D FooD gramps/parent/child
  $ test -d gramps/parent
  $ test -f gramps/parent/child
  $ test -e FooD
  $ rm -r FooD gramps

install -D, multi src
  $ test ! -e gran
  $ touch sis bro
  $ install -D sis bro gran/dad
  $ test -f sis
  $ test -f bro
  $ test -d gran/dad
  $ test -f gran/dad/sis
  $ test -f gran/dad/bro
  $ rm -r sis bro gran

install -T
  $ test ! -e src_T
  $ touch src_T
  $ test -e src_T
  $ install -T src_T dest_T
  $ test -e src_T
  $ test -e dest_T
  $ rm dest_T
  $ install -T src_T src2_T bogus_T
  install: error: Option -T passed, expected exactly 1 source operand, got 2
  [1]
  $ test -e src_T
  $ test ! -e src2_T
  $ test ! -e bogus_T
  $ rm src_T

install -t
  $ touch src1_t src2_t
  $ test -e src1_t
  $ test -e src2_t
  $ mkdir dest_t
  $ test -d dest_t
  $ test ! -e dest_t/src1_t
  $ test ! -e dest_t/src2_t
  $ install -t dest_t src1_t src2_t
  $ test -e src1_t
  $ test -e src2_t
  $ test -e dest_t/src1_t
  $ test -e dest_t/src2_t
  $ rm -r dest_t src1_t src2_t

No files should be left
  $ find .
  .
