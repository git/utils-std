#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/date"
plans=33
. "$(dirname "$0")/tap.sh"
. "$(dirname "$0")/init_env.sh"

export TZ=UTC
export LC_ALL=C

t 'epoch' '-u -d @1155544496 +%s' '1155544496
'
t 'rfc3339' '-u -d @1155544496 +%FT%T%z' '2006-08-14T08:34:56+0000
'
t 'rfc5322' '-u -d @1155544496 -R' 'Mon, 14 Aug 2006 08:34:56 +0000
'
t 'empty' '+' '
'
t 'echolike' '+hello_world' 'hello_world
'
t '@0' '-u -d @0 +%FT%T' '1970-01-01T00:00:00
'
t '@69' '-u -d @69 +%FT%T' '1970-01-01T00:01:09
'
t '@-69' '-u -d @-69 +%FT%T' '1969-12-31T23:58:51
'

t '1970-01-01T00:00:00Z' '-u -d 1970-01-01T00:00:00Z +%s' '0
'
t '1970-01-01T00:01:09Z' '-u -d 1970-01-01T00:01:09Z +%s' '69
'
t '1969-12-31T23:58:51Z' '-u -d 1969-12-31T23:58:51Z +%s' '-69
'
t '1969-12-31T23:58:51.00Z' '-u -d 1969-12-31T23:58:51.00Z +%s' '-69
'
t '1969-12-31T23:58:51,00Z' '-u -d 1969-12-31T23:58:51,00Z +%s' '-69
'

t 'f_option' '-u -j -f %Y-%m-%dT%H:%M:%S 2006-08-14T08:34:56 +%s' '1155544496
'

# Note the lack of setting seconds
t 'mmddHHMM' '-j 07250554 +%Y-%m-%dT%H:%M' "$(date +%Y-07-25T05:54)
"
t 'mmddHHMM24' '-j 0725055424 +%Y-%m-%dT%H:%M' '2024-07-25T05:54
'
t 'mmddHHMM69' '-j 0628012069 +%Y-%m-%dT%H:%M' '1969-06-28T01:20
'
t 'mmddHHMMCCyy' '-j 072505542024 +%Y-%m-%dT%H:%M' '2024-07-25T05:54
'

t 'r_0' '-u -r 0 +%FT%T' '1970-01-01T00:00:00
'
t 'r_69' '-u -r 69 +%FT%T' '1970-01-01T00:01:09
'
t 'r_-69' '-u -r -69 +%FT%T' '1969-12-31T23:58:51
'

t 'iso Date' '-u -d 2025-02-10T21:05:53,437742835+01:00 -Idate' '2025-02-10
'

t 'iso Hours' '-u -d 2025-02-10T21:05:53,437742835+01:00 -Ihours' '2025-02-10T20+00:00
'

t 'iso Minutes' '-u -d 2025-02-10T21:05:53,437742835+01:00 -Iminutes' '2025-02-10T20:05+00:00
'

t 'iso Seconds' '-u -d 2025-02-10T21:05:53,437742835+01:00 -Iseconds' '2025-02-10T20:05:53+00:00
'

t_args 'iso seconds-space-tz' '2025-02-10T20:05:53+00:00
' -u -d '2025-02-10 21:05:53,437742835 +01:00' -Iseconds

t 'iso Nano-Seconds' '-u -d 2025-02-10T21:05:53,437742835+01:00 -Ins' '2025-02-10T20:05:53,437742835+00:00
'

t '+foo%%bar' '+foo%%bar' 'foo%bar
'

t_args 'email RFC2822-RFC5322-example1' '1997-11-21 15:55:06+0000
' -u -d 'Fri, 21 Nov 1997 09:55:06 -0600' '+%F %T%z'

# date -u -d '1969-02-13 23:32:00-0330' '+%F %T%z'
t_args 'email RFC2822-RFC5322-whitespace-oddities' '1969-02-14 03:02:00+0000
' -u -d 'Thu,
      13
       Feb
         1969
      23:32
             -0330' '+%F %T%z'

t_args 'email RFC2822-RFC5322-obsolete-dates' '1997-11-21 09:55:06+0000
' -u -d '21 Nov 97 09:55:06 GMT' '+%F %T%z'

t_args 'email Y2K' '2017-11-21 09:55:06+0000
' -u -d '21 Nov 17 09:55:06 GMT' '+%F %T%z'

t_args 'asctime' '1973-09-16 01:03:52+0000
' -u -d 'Sun Sep 16 01:03:52 1973' '+%F %T%z'

#usage="\
#date [-uR] [-d datetime] [+format]
#date [-uR] -f now_format now [+format]
#"
#	atf_check -s 'exit:1' -e "inline:date: Error: Unrecognised option: '-x'\n${usage}" ../cmd/date -x

#atf_test_case devfull
#devfull_body() {
#	has_glibc && atf_skip "glibc ignoring write errors for puts()"
#	[ "$(uname -s)" = "NetBSD" ] && atf_skip "NetBSD ignoring write errors for puts()"
#	[ "$(uname -s)" = "FreeBSD" ] && atf_skip "FreeBSD ignoring write errors for puts()"
#
#	atf_check -s exit:1 -e 'inline:date: puts: No space left on device\n' sh -c '../cmd/date >/dev/full'
#}

#atf_test_case utc
#utc_body() {
#	atf_check -o "match:^[0-9]+$" ../cmd/date -u '+%s'
#}

#atf_test_case timestamp
#timestamp_body() {
#	atf_check -s 'exit:1' -e "inline:date: Error: Missing operand for option: '-d'\n${usage}" ../cmd/date -u -d
#
#	# 36893488147419103232 = 2^65
#	atf_check -s 'exit:1' -e not-empty ../cmd/date -u -d @36893488147419103232
#}
