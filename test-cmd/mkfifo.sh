#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/mkfifo"
plans=7
. "${WD}/tap.sh"

t foo foo
t_cmd foo:test '' test -p foo
t_cmd foo:rm '' rm foo

t_cmd exists:touch '' touch exists
t --exit=1 exists exists "mkfifo: error: Failed creating FIFO at 'exists': File exists
"
t_cmd exists:rm '' rm exists

t --exit=1 enotdir /dev/null/e/noent "mkfifo: error: Failed creating FIFO at '/dev/null/e/noent': Not a directory
"
