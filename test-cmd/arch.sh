#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/arch"
plans=1
. "$(dirname "$0")/tap.sh"

t 'no args' '' "$(uname -m)
"
