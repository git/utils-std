#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/id"
plans=11
. "$(dirname "$0")/tap.sh"

usage='Usage: id [-Ggu] [-nr] [user]
'

t g -g "$(id -g)
"
t gr -gr "$(id -gr)
"
t gn -gn "$(id -gn)
"
t gnr -gnr "$(id -gnr)
"

t u -u "$(id -u)
"
t ur -ur "$(id -ur)
"
t un -un "$(id -un)
"
t unr -unr "$(id -unr)
"

# Make sure the correct list is returned for different users
# Previously it would only return the runtime list of the current user
t G_nobody '-G nobody' "$(id -G nobody)
"
t G_root '-G root' "$(id -G root)
"

t --exit=1 badarg '-a' "id: error: Unhandled option '-a'
${usage}"
