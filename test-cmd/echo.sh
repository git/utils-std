#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

target="$(dirname "$0")/../cmd/echo"
plans=7
. "$(dirname "$0")/tap.sh"

t 'empty' '' '
'
t 'hello' 'hello' 'hello
'
t '-- hello' '-- hello' '-- hello
'

t -- '-n' '-n' ''
t -- '-n foo' '-n foo' 'foo'
t -- '-n foo bar' '-n foo bar' 'foo bar'
t -- '-n -- foo' '-n -- foo' '-- foo'
