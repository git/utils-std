#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")/../"
target="${WD}/cmd/pathchk"
plans=13

. "${WD}/test-cmd/tap.sh"

# Thanks glibc for the broken getconf
POSIX_NAME_MAX=$(getconf _POSIX_NAME_MAX 2>/dev/null || echo 14)
POSIX_PATH_MAX=$(getconf _POSIX_PATH_MAX 2>/dev/null || echo 256)

# Get NAME_MAX and PATH_MAX from <limits.h> (getconf doesn't always matches, such as on NetBSD)
if ! test -f "${WD}/test-cmd/pathchk-getlimits"; then
	echo "${WD}/test-cmd/pathchk-getlimits: Is missing, did you use make?"
	exit 1
fi
export $("${WD}/test-cmd/pathchk-getlimits")
echo "# NAME_MAX: ${NAME_MAX?}"
echo "# PATH_MAX: ${PATH_MAX?}"

name_max_str=$(printf "%*s" "${NAME_MAX}" | tr ' ' _)
t name_max:ok "foo/${name_max_str}/bar"
t --exit=1 name_max:xfail "foo/s${name_max_str}/bar" "pathchk: error: Path component ($(( ${NAME_MAX} + 1 )) octets) is over the maximum size (${NAME_MAX} octets): s${name_max_str}
"

posix_name_max_str=$(printf "%*s" "${POSIX_NAME_MAX}" | tr ' ' _)
t posix_name_max:ok "-p foo/${posix_name_max_str}/bar"
t --exit=1 posix_name_max:xfail "-p foo/s${posix_name_max_str}/bar" "pathchk: error: Path component ($(( ${POSIX_NAME_MAX} + 1 )) octets) is over the maximum size (${POSIX_NAME_MAX} octets): s${posix_name_max_str}
"

path_max_str=$(printf "%*s" "$(( ${PATH_MAX} - 1 ))" | tr ' ' /)
t path_max:ok "${path_max_str}"
t --exit=1 path_max:xfail "s${path_max_str}" "pathchk: error: Path (${PATH_MAX} octets) is over the maximum size ($(( ${PATH_MAX} - 1 )) octets): s${path_max_str}
"

posix_path_max_str=$(printf "%*s" "$(( ${POSIX_PATH_MAX} - 1 ))" | tr ' ' /)
t posix_path_max:ok "-p ${posix_path_max_str}"
t --exit=1 posix_path_max:xfail "-p s${posix_path_max_str}" "pathchk: error: Path (${POSIX_PATH_MAX} octets) is over the maximum size ($(( ${POSIX_PATH_MAX} - 1 )) octets): s${posix_path_max_str}
"

t enoent /dev/null/foo
t noparent /../foo

t --exit=1 hyphen '-P ./-foo' 'pathchk: error: Path component starts with an hyphen: -foo
'
t --exit=1 hyphen_subdir '-P ./bar/-foo' 'pathchk: error: Path component starts with an hyphen: -foo
'

zero_two="$(printf '\02')"
t --exit=1 posix_charset "-p foo${zero_two}bar" "pathchk: error: Non-portable character '${zero_two}' (0x02) found in: foo${zero_two}bar
"
