#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0
WD=$(dirname "$0")
plans=17
target="${WD}/../cmd/tee"
. "${WD}/tap.sh"

t_file --infile="${WD}/inputs/all_bytes" all_bytes "${WD}/inputs/all_bytes"

tmp_writefile="$(mktemp)"
echo hello > "${tmp_writefile}"
t_cmd writefile:grep '' grep -q hello "${tmp_writefile}"
t_file --infile="${WD}/inputs/all_bytes" writefile "${WD}/inputs/all_bytes" "${tmp_writefile}"
t_cmd writefile:cmp '' cmp "${tmp_writefile}" "${WD}/inputs/all_bytes"
t_cmd writefile:rm '' rm "${tmp_writefile}"

tmp_appendfile="$(mktemp)"
echo hello > "${tmp_appendfile}"
t_file --infile="${WD}/inputs/all_bytes" appendfile "${WD}/inputs/all_bytes" -a "${tmp_appendfile}"
t_cmd writefile:cmp '' cmp "${tmp_appendfile}" "${WD}/outputs/tee/hello_all_bytes"
t_cmd appendfile:rm '' rm "${tmp_appendfile}"

if [ "x${LOGNAME}" = 'xroot' ]; then
	skip noperm:chmod_0000 'ran as root, ignoring'
	skip noperm 'ran as root, ignoring'
	skip noperm:chmod_0600 'ran as root, ignoring'
	skip noperm:rm 'ran as root, ignoring'
else
	tmp_noperm="$(mktemp)"
	t_cmd noperm:chmod_0000 '' chmod 0000 "${tmp_noperm}"
	t --exit=1 noperm "${tmp_noperm}" "tee: error: Failed opening file ‘${tmp_noperm}’: Permission denied
" </dev/null
	t_cmd noperm:chmod_0600 '' chmod 0600 "${tmp_noperm}"
	t_cmd noperm:rm '' rm "${tmp_noperm}"
fi

t --input='' nullinput

t --exit=1 --input='foo' writeslash './' 'tee: error: Failed opening file ‘./’: Is a directory
'

t --exit=1 --input='foo' enoent '/var/empty/e/no/ent' 'tee: error: Failed opening file ‘/var/empty/e/no/ent’: No such file or directory
'

t_file --infile="${WD}/inputs/all_bytes" doubledash "${WD}/inputs/all_bytes" --

t --exit=1 --input='foo' tripledash '---' "tee: error: Unrecognised option: '--'
"
