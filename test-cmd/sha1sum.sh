#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

WD="$(dirname "$0")"
target="${WD}/../cmd/sha1sum"
plans=16
. "$(dirname "$0")/tap.sh"

if test "$(uname -s)" = "FreeBSD"
then
	skip devnull 'FreeBSD treats posix_fadvise on /dev/null as invalid'
else
	t devnull '/dev/null' 'da39a3ee5e6b4b0d3255bfef95601890afd80709  /dev/null
'
fi
t empty "$WD/inputs/empty" "da39a3ee5e6b4b0d3255bfef95601890afd80709  $WD/inputs/empty
"
t --input='' 'empty_stdin' '' 'da39a3ee5e6b4b0d3255bfef95601890afd80709
'

t --input='abc' 'abc' '' 'a9993e364706816aba3e25717850c26c9cd0d89d
'

t --exit=1 'enoent' '/var/empty/e/no/ent' "sha1sum: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"

t --input="da39a3ee5e6b4b0d3255bfef95601890afd80709  $WD/inputs/empty" 'check:empty' '-c' "$WD/inputs/empty: OK
"
t --input="da39a3ee5e6b4b0d3255bfef95601890afd80709 *$WD/inputs/empty" 'bin_check:empty' '-c' "$WD/inputs/empty: OK
"
t --input="da39a3ee5e6b4b0d3255bfef95601890afd80709 *$WD/inputs/empty" 'bin_check:empty:dash' '-c -' "$WD/inputs/empty: OK
"

t --exit=1 --input="a9993e364706816aba3e25717850c26c9cd0d89d  $WD/inputs/empty" 'xfail_check:empty' '-c' "$WD/inputs/empty: FAILED
"
t --exit=1 --input="a9993e364706816aba3e25717850c26c9cd0d89d *$WD/inputs/empty" 'xfail_bin_check:empty' '-c' "$WD/inputs/empty: FAILED
"

t --exit=1 --input='a9993e364706816aba3e25717850c26c9cd0d89d  /var/empty/e/no/ent' 'xfail_check:enoent' '-c' "sha1sum: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"
t --exit=1 --input='a9993e364706816aba3e25717850c26c9cd0d89d */var/empty/e/no/ent' 'xfail_bin_check:enoent' '-c' "sha1sum: error: Failed opening file '/var/empty/e/no/ent': No such file or directory
"

t --exit=1 --input="# $WD/inputs/empty" 'invalid_chars:#' '-c' "sha1sum: error: Invalid character '#' while reading hash in line: # $WD/inputs/empty
"
t --exit=1 --input="a9993e364706816aba3e25717850c26c9cd0d89d" 'missing_file' '-c' "sha1sum: error: Failed opening file '': No such file or directory
"
t --exit=1 --input="a9993e364706816aba3e25717  $WD/inputs/empty" 'truncated_hash' '-c' 'sha1sum: error: Got 26 hexadecimal digits while expected 41 for a SHA1
'
t --exit=1 --input="a9993e364706816aba3e25717850c26c9cd0d89da9993e364706816aba3e25717850c26c9cd0d89d  $WD/inputs/empty" 'elongated_hash' '-c' 'sha1sum: error: Got 81 hexadecimal digits while expected 41 for a SHA1
'
