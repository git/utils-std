// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#define _POSIX_C_SOURCE 200809L
#include "../lib/sha512.h"
#include "../lib/strconv.h"

#include <assert.h>
#include <stdio.h>  // printf
#include <string.h> // memcmp

int counter = 0;
int err = 0;

static void
t_sha512(const char *name,
         const void *data,
         size_t datalen,
         const char expected[SHA512_DIGEST_LENGTH * 2])
{
	int id = ++counter;

	struct sha512 ctx;
	sha512_init(&ctx);
	sha512_update(&ctx, data, datalen);

	uint8_t res[SHA512_DIGEST_LENGTH] = "";
	sha512_sum(&ctx, res);

	char got[SHA512_DIGEST_LENGTH * 2] = "";
	bytes2hex(res, sizeof(res), got, sizeof(got));

	if(memcmp(expected, got, SHA512_DIGEST_LENGTH * 2) == 0)
	{
		printf("ok %d - %s\n", id, name);
		return;
	}

	err = 1;

	fputs("# Expected: ", stdout);
	fwrite(expected, SHA512_DIGEST_LENGTH * 2, 1, stdout);
	fputs("\n", stdout);

	fputs("# Got: ", stdout);
	fwrite(got, SHA512_DIGEST_LENGTH * 2, 1, stdout);
	fputs("\n", stdout);

	printf("not ok %d - %s\n", id, name);
}

int
main(void)
{
	int plan = 3;
	printf("1..%d\n", plan);

	t_sha512("empty",
	         "",
	         0,
	         "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce"
	         "47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e");

	// https://web.archive.org/web/20130526224224/https://csrc.nist.gov/groups/STM/cavp/documents/shs/sha512-384-512.pdf
	t_sha512("abc",
	         "abc",
	         3,
	         "ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a"
	         "2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f");
	// clang-format off
	t_sha512("abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu",
	         "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu",
	         112,
	         "8e959b75dae313da8cf4f72814fc143f8f7779c6eb9f7fa17299aeadb6889018"
	         "501d289e4900f7e4331b99dec4b5433ac7d329eeb6dd26545e96e55b874be909");
	// clang-format on

	assert(counter == plan);
	return err;
}
