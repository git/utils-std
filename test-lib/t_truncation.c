// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L
#include "../lib/truncation.h"

#include <assert.h>
#include <stdio.h> // printf

const char *argv0 = "test-lib/truncation";

int counter = 0;
int err = 0;

static void
t_parse_size(const char *str, off_t size, enum operation_e op)
{
	int id = ++counter;

	struct truncation tr;
	int ret = parse_size(str, &tr);
	if(ret == 0 && tr.size == size && tr.op == op)
	{
		printf("ok %d - parse_size(\"%s\", _) -> {.size = %ld, .op = %d}\n", id, str, tr.size, tr.op);
		return;
	}

	err = 1;
	printf("not ok %d - parse_size(\"%s\", _) -> {.size = %ld, .op = %d}\n", id, str, tr.size, tr.op);
	if(tr.size != size || tr.op != op) printf("# Expected: {.size = %ld, .op = %d}\n", size, op);
	if(ret != 0) printf("# Exit status: %d\n", ret);
}

static void
set(void)
{
	t_parse_size("0", 0L, OP_SET);
	t_parse_size("666", 666L, OP_SET);
	t_parse_size("666M", 666 * 1024 * 1024L, OP_SET);
	t_parse_size("666MB", 666 * 1000 * 1000L, OP_SET);
	t_parse_size("666MiB", 666 * 1024 * 1024L, OP_SET);
}

static void
inc(void)
{
	t_parse_size("+0", 0L, OP_INC);
	t_parse_size("+666", 666L, OP_INC);
	t_parse_size("+666M", 666 * 1024 * 1024L, OP_INC);
	t_parse_size("+666MB", 666 * 1000 * 1000L, OP_INC);
	t_parse_size("+666MiB", 666 * 1024 * 1024L, OP_INC);
}

static void
dec(void)
{
	t_parse_size("-0", 0L, OP_DEC);
	t_parse_size("-666", 666L, OP_DEC);
	t_parse_size("-666M", 666 * 1024 * 1024L, OP_DEC);
	t_parse_size("-666MB", 666 * 1000 * 1000L, OP_DEC);
	t_parse_size("-666MiB", 666 * 1024 * 1024L, OP_DEC);
}

int
main(void)
{
	int plan = 15;
	printf("1..%d\n", plan);

	// needs standard error capture
	//t_parse_size("", 0, OP_SET);

	set();
	inc();
	dec();

	assert(counter == plan);
	return err;
}
