// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#define _POSIX_C_SOURCE 200809L
#include "../lib/sha256.h"
#include "../lib/strconv.h"

#include <assert.h>
#include <stdio.h>  // printf
#include <string.h> // memcmp

int counter = 0;
int err = 0;

static void
t_sha256(const char *name,
         const void *data,
         size_t datalen,
         const char expected[SHA256_DIGEST_LENGTH * 2])
{
	int id = ++counter;

	struct sha256 ctx;
	sha256_init(&ctx);
	sha256_update(&ctx, data, datalen);

	uint8_t res[SHA256_DIGEST_LENGTH] = "";
	sha256_sum(&ctx, res);

	char got[SHA256_DIGEST_LENGTH * 2] = "";
	bytes2hex(res, sizeof(res), got, sizeof(got));

	if(memcmp(expected, got, SHA256_DIGEST_LENGTH * 2) == 0)
	{
		printf("ok %d - %s\n", id, name);
		return;
	}

	err = 1;

	fputs("# Expected: ", stdout);
	fwrite(expected, SHA256_DIGEST_LENGTH * 2, 1, stdout);
	fputs("\n", stdout);

	fputs("# Got: ", stdout);
	fwrite(got, SHA256_DIGEST_LENGTH * 2, 1, stdout);
	fputs("\n", stdout);

	printf("not ok %d - %s\n", id, name);
}

int
main(void)
{
	int plan = 3;
	printf("1..%d\n", plan);

	t_sha256("empty", "", 0, "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");

	// https://web.archive.org/web/20130526224224/https://csrc.nist.gov/groups/STM/cavp/documents/shs/sha256-384-512.pdf
	t_sha256("abc", "abc", 3, "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad");
	t_sha256("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
	         "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
	         56,
	         "248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1");

	assert(counter == plan);
	return err;
}
