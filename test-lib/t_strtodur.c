// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L
#include "../lib/strtodur.h"

#include <assert.h>
#include <stdio.h> // printf

const char *argv0 = "t_strtodur";
int counter = 0;
int err = 0;

static void
t_strtodur(char *str, time_t ex_sec, long ex_nsec)
{
	int id = ++counter;

	struct timespec dur = {.tv_sec = 0, .tv_nsec = 0};
	int ret = strtodur(str, &dur);

	if(dur.tv_sec == ex_sec && dur.tv_nsec == ex_nsec && ret == 0)
	{
		printf("ok %d - strtodur(\"%s\", _) -> {.tv_sec = %ld, .tv_nsec = %ld}\n",
		       id,
		       str,
		       dur.tv_sec,
		       dur.tv_nsec);
		return;
	}

	err = 1;
	printf("not ok %d - strtodur(\"%s\", _) -> {.tv_sec = %ld, .tv_nsec = %ld}\n",
	       id,
	       str,
	       dur.tv_sec,
	       dur.tv_nsec);
	if(dur.tv_sec != ex_sec || dur.tv_nsec != ex_nsec)
		printf("# Expected: {.tv_sec = %ld, .tv_nsec = %ld}\n", ex_sec, ex_nsec);
	if(ret != 0) printf("# Exit status: %d\n", ret);
}

int
main(void)
{
	int plan = 16;
	printf("1..%d\n", plan);

	// TODO: Capture errors, say with open_memstream(3)

	t_strtodur(NULL, 0, 0);
	t_strtodur((char *)"", 0, 0);
	t_strtodur((char *)",", 0, 0);

	t_strtodur((char *)"1", 1, 0);
	t_strtodur((char *)"1.", 1, 0);
	t_strtodur((char *)"1,", 1, 0);

	t_strtodur((char *)".1", 0, 1000000000 * 0.1);
	t_strtodur((char *)"0.1", 0, 1000000000 * 0.1);

	t_strtodur((char *)"1s", 1, 0);
	t_strtodur((char *)"1m", 60, 0);
	t_strtodur((char *)"1h", 60 * 60, 0);
	t_strtodur((char *)"1d", 60 * 60 * 24, 0);

	t_strtodur((char *)"1.5s", 1, 1000000000 * 0.5);
	t_strtodur((char *)"1.5m", 1.5 * 60, 0);
	t_strtodur((char *)"1.5h", 1.5 * 60 * 60, 0);
	t_strtodur((char *)"1.5d", 1.5 * 60 * 60 * 24, 0);

	assert(counter == plan);
	return err;
}
