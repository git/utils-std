// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#define _POSIX_C_SOURCE 200809L
#include "../lib/sha1.h"
#include "../lib/strconv.h"

#include <assert.h>
#include <stdio.h>  // printf
#include <string.h> // memcmp, memset

int counter = 0;
int err = 0;

static void
t_sha1(const char *name,
       const void *data,
       size_t datalen,
       const char expected[SHA1_DIGEST_LENGTH * 2])
{
	int id = ++counter;

	struct sha1 ctx;
	sha1_init(&ctx);
	sha1_update(&ctx, data, datalen);

	uint8_t res[SHA1_DIGEST_LENGTH] = "";
	sha1_sum(&ctx, res);

	char got[SHA1_DIGEST_LENGTH * 2] = "";
	bytes2hex(res, sizeof(res), got, sizeof(got));

	if(memcmp(expected, got, SHA1_DIGEST_LENGTH * 2) == 0)
	{
		printf("ok %d - %s\n", id, name);
		return;
	}

	err = 1;

	fputs("# Expected: ", stdout);
	fwrite(expected, SHA1_DIGEST_LENGTH * 2, 1, stdout);
	fputs("\n", stdout);

	fputs("# Got: ", stdout);
	fwrite(got, SHA1_DIGEST_LENGTH * 2, 1, stdout);
	fputs("\n", stdout);

	printf("not ok %d - %s\n", id, name);
}

int
main(void)
{
	int plan = 3;
	printf("1..%d\n", plan);

	uint8_t million_a[1000000];
	memset(million_a, 'a', 1000000);

	t_sha1("empty", "", 0, "da39a3ee5e6b4b0d3255bfef95601890afd80709");
	t_sha1("abc", "abc", 3, "a9993e364706816aba3e25717850c26c9cd0d89d");
	t_sha1("million_a", million_a, 1000000, "34aa973cd4c4daa4f61eeb2bdbad27316534016f");

	assert(counter == plan);
	return err;
}
