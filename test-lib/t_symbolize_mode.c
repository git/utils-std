// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L
#include "../lib/mode.h"

#include <assert.h>
#include <stdio.h>  // printf
#include <string.h> // strcmp

int counter = 0;
int err = 0;

static void
t_symbolize_mode(mode_t mode, const char *expected)
{
	char str[11] = "";
	symbolize_mode(mode, str);
	if(strcmp(str, expected) == 0)
	{
		printf("ok %d - %7o -> %s\n", ++counter, mode, expected);
		return;
	}

	err = 1;
	printf("not ok %d - %7o -> %s\n", ++counter, mode, expected);
	printf("# Got: %s\n", str);
}

int
main(void)
{
	int plan = 6;
	printf("1..%d\n", plan);

	t_symbolize_mode(0040000, "d---------");
	t_symbolize_mode(0040755, "drwxr-xr-x");
	t_symbolize_mode(0020644, "crw-r--r--");

	t_symbolize_mode(0024644, "crwSr--r--");
	t_symbolize_mode(0022774, "crwxrwsr--");
	t_symbolize_mode(0021744, "crwxr--r-T");

	assert(counter == plan);
	return err;
}
