#!/bin/sh
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

set -o pipefail

WD="$(realpath "$(dirname "$0")")/"
log="${WD}/check-libs.log"
err=0
. "${WD}/check-funcs.sh"

cd "$WD"

rm "$log"

if [ "$#" -gt 0 ]
then
	for runner
	do
		wrap_test "$runner"
	done
else
	for src in test-lib/*.c
	do
		runner="${src%.c}"
		wrap_test "$runner"
	done
fi

[ $err = 0 ] && rm "$log"
exit $err
