#!/bin/false
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

set -o pipefail

tmpdir=

run_test() {
	env MALLOC_CHECK_=3 POSIX_ME_HARDER=1 POSIXLY_CORRECT=1 LC_ALL=C.UTF-8 "$1" 2>&1
}

wrap_test() {
	runner="$1"
	echo "${PS4}${runner}"

	if command -v mktemp 2>/dev/null >/dev/null
	then
		cd "$WD"
		tmpdir="$(mktemp -d selfcheck.XXXXXX)"
		cd "$tmpdir"
	fi

	run_test "${WD}/${runner}" | tee -a "${log}" | grep -v '^ok'
	ret="$?"
	if [ "$ret" != 0 ]
	then
		printf '## %s exited with code %s\n' "$runner" "$ret"
		err=1
	fi

	remains="$(jobs -l)"
	if [ "${remains}" != "" ]; then
		printf '## %s: ' 'Some processes remaining'
		echo "$remains" | tr '\n' ';'
		printf '\n## %s\n' 'Waiting for them to exit'
		wait
		echo "## Done"
	fi

	if [ -n "$tmpdir" ]
	then
		cd "$WD"
		if ! rm -d "$tmpdir"
		then
			printf '## Error: %s left files:\n%s\n' "$runner" "$(ls -R "$tmpdir")"
			err=1
		fi
	fi
}
