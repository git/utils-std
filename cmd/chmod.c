// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L

// NetBSD <10 hides fdopendir behind _NETBSD_SOURCE
#if __NetBSD_Version__ < 1000000000
#define _NETBSD_SOURCE
#endif

#include "../config.h" // HAS_*
#include "../lib/mode.h"

#include <dirent.h> // fdopendir, readdir, closedir
#include <errno.h>
#include <fcntl.h>  // AT_FDCWD
#include <limits.h> // PATH_MAX
#include <stdbool.h>
#include <stdio.h>    // fprintf
#include <string.h>   // strerror
#include <sys/stat.h> // chmod, fstatat, S_ISDIR
#include <unistd.h>   // getopt
#ifdef HAS_GETOPT_LONG
#include <getopt.h>
#endif

const char *argv0 = "chmod";
bool opt_c = false, opt_v = false;

static int
do_fchmodat(int fd, char *mode_arg, char *name, char *acc_path, bool recursive)
{
	struct stat stats;
	int err = 0;

	if(fstatat(fd, name, &stats, AT_SYMLINK_NOFOLLOW) != 0)
	{
		fprintf(stderr,
		        "%s: error: Failed getting status for '%s': %s\n",
		        argv0,
		        acc_path,
		        strerror(errno));
		errno = 0;
		return 1;
	}

	const char *errstr = NULL;
	mode_t mode = new_mode(mode_arg, stats.st_mode, &errstr);
	if(errstr != NULL)
	{
		fprintf(stderr, "%s: error: Failed parsing mode '%s': %s\n", argv0, mode_arg, errstr);
		return 1;
	}

	char mode_from[11] = "";
	symbolize_mode(stats.st_mode, mode_from);

	if(mode != stats.st_mode)
	{
		if(fchmodat(fd, name, mode, 0) != 0)
		{
			fprintf(stderr,
			        "%s: error: Failed setting permissions to 0%04o for '%s': %s\n",
			        argv0,
			        mode,
			        acc_path,
			        strerror(errno));
			errno = 0;
			return 1;
		}

		if(opt_c || opt_v)
		{
			char mode_to[11] = "";
			symbolize_mode(mode, mode_to);

			printf("%s: Permissions changed from 0%04o/%s to 0%04o/%s for '%s'\n",
			       argv0,
			       stats.st_mode & 07777,
			       mode_from,
			       mode & 07777,
			       mode_to,
			       acc_path);
		}
	}
	else if(opt_v)
		printf("%s: Permissions already set to 0%04o/%s for '%s'\n",
		       argv0,
		       stats.st_mode & 07777,
		       mode_from,
		       acc_path);

	if(recursive && S_ISDIR(stats.st_mode))
	{
		int dir = openat(fd, name, O_RDONLY | O_DIRECTORY | O_CLOEXEC);
		if(dir == -1)
		{
			fprintf(stderr,
			        "%s: error: Couldn't open '%s' as directory: %s\n",
			        argv0,
			        acc_path,
			        strerror(errno));
			errno = 0;
			return 1;
		}

		DIR *dirp = fdopendir(dir);
		if(dirp == NULL)
		{
			fprintf(stderr,
			        "%s: error: Couldn't get DIR entry for opened '%s': %s\n",
			        argv0,
			        acc_path,
			        strerror(errno));
			errno = 0;
			return 1;
		}

		while(true)
		{
			struct dirent *dp = readdir(dirp);
			if(dp == NULL)
			{
				if(errno == 0) break;

				fprintf(stderr,
				        "%s: error: Failed reading directory '%s': %s\n",
				        argv0,
				        acc_path,
				        strerror(errno));
				closedir(dirp); // FIXME: unhandled error
				errno = 0;
				return 1;
			}

			if(strcmp(dp->d_name, ".") == 0) continue;
			if(strcmp(dp->d_name, "..") == 0) continue;

			char new_path[PATH_MAX] = "";
			if(snprintf(new_path, PATH_MAX, "%s/%s", acc_path, dp->d_name) < 0)
			{
				fprintf(
				    stderr,
				    "%s: error: Couldn't concatenate '%s' into parent '%s', skipping to next entry: %s\n",
				    argv0,
				    name,
				    acc_path,
				    strerror(errno));
				err++;
				errno = 0;
				continue;
			}

			// No depth counter for now, unlikely to be a problem
			int ret = do_fchmodat(dir, mode_arg, dp->d_name, new_path, true);
			if(ret != 0) return ret;
		}

		// fdopendir allocates memory for DIR, needs closedir
		if(closedir(dirp) != 0)
		{
			fprintf(stderr,
			        "%s: error: Deallocating directory entry for '%s' failed: %s\n",
			        argv0,
			        acc_path,
			        strerror(errno));
			errno = 0;
			return 1;
		}
	}

	return err;
}

static int
copy_mode(int fd, mode_t mode, char *name, char *acc_path, bool recursive)
{
	struct stat stats;
	int err = 0;

	if(fstatat(fd, name, &stats, AT_SYMLINK_NOFOLLOW) != 0)
	{
		fprintf(stderr,
		        "%s: error: Failed getting status for '%s': %s\n",
		        argv0,
		        acc_path,
		        strerror(errno));
		errno = 0;
		return 1;
	}

	char mode_from[11] = "";
	symbolize_mode(stats.st_mode, mode_from);

	if(mode != stats.st_mode)
	{
		if(fchmodat(fd, name, mode, 0) != 0)
		{
			fprintf(stderr,
			        "%s: error: Failed setting permissions to 0%04o for '%s': %s\n",
			        argv0,
			        mode,
			        acc_path,
			        strerror(errno));
			errno = 0;
			return 1;
		}

		if(opt_c || opt_v)
		{
			char mode_to[11] = "";
			symbolize_mode(mode, mode_to);

			printf("%s: Permissions changed from 0%04o/%s to 0%04o/%s for '%s'\n",
			       argv0,
			       stats.st_mode & 07777,
			       mode_from,
			       mode & 07777,
			       mode_to,
			       acc_path);
		}
	}
	else if(opt_v)
		printf("%s: Permissions already set to 0%04o/%s for '%s'\n",
		       argv0,
		       stats.st_mode & 07777,
		       mode_from,
		       acc_path);

	if(recursive && S_ISDIR(stats.st_mode))
	{
		int dir = openat(fd, name, O_RDONLY | O_DIRECTORY | O_CLOEXEC);
		if(dir == -1)
		{
			fprintf(stderr,
			        "%s: error: Couldn't open '%s' as directory: %s\n",
			        argv0,
			        acc_path,
			        strerror(errno));
			errno = 0;
			return 1;
		}

		DIR *dirp = fdopendir(dir);
		if(dirp == NULL)
		{
			fprintf(stderr,
			        "%s: error: Couldn't get DIR entry for opened '%s': %s\n",
			        argv0,
			        acc_path,
			        strerror(errno));
			errno = 0;
			return 1;
		}

		while(true)
		{
			struct dirent *dp = readdir(dirp);
			if(dp == NULL)
			{
				if(errno == 0) break;

				fprintf(stderr,
				        "%s: error: Failed reading directory '%s': %s\n",
				        argv0,
				        acc_path,
				        strerror(errno));
				closedir(dirp); // FIXME: unhandled error
				errno = 0;
				return 1;
			}

			if(strcmp(dp->d_name, ".") == 0) continue;
			if(strcmp(dp->d_name, "..") == 0) continue;

			char new_path[PATH_MAX] = "";
			if(snprintf(new_path, PATH_MAX, "%s/%s", acc_path, dp->d_name) < 0)
			{
				fprintf(
				    stderr,
				    "%s: error: Couldn't concatenate '%s' into parent '%s', skipping to next entry: %s\n",
				    argv0,
				    name,
				    acc_path,
				    strerror(errno));
				err++;
				errno = 0;
				continue;
			}

			// No depth counter for now, unlikely to be a problem
			int ret = copy_mode(dir, mode, dp->d_name, new_path, true);
			if(ret != 0) return ret;
		}

		// fdopendir allocates memory for DIR, needs closedir
		if(closedir(dirp) != 0)
		{
			fprintf(stderr,
			        "%s: error: Deallocating directory entry for '%s' failed: %s\n",
			        argv0,
			        acc_path,
			        strerror(errno));
			errno = 0;
			return 1;
		}
	}

	return err;
}

static void
usage(void)
{
	fprintf(stderr, "\
Usage: chmod [-cRv] <mode> <file...>\n\
       chmod [-cRv] -F <ref_file> <file...>\n");
}

int
main(int argc, char *argv[])
{
	bool opt_R = false;
	char *ref_file = NULL;

#ifdef HAS_GETOPT_LONG
	// clang-format off
	enum long_opt_vals {
		OPT_REFERENCE = 1,
	};
	static struct option opts[] = {
		{"changes", no_argument, 0, 'c'},
		{"recursive", no_argument, 0, 'R'},
		{"reference", required_argument, 0, OPT_REFERENCE},
		{"verbose", no_argument, 0, 'v'},
		{0, 0, 0, 0},
	};
	// clang-format on
#endif

	for(int c = -1;;)
	{
		if(optind >= argc || !argv[optind]) break;

		if(argv[optind][0] == '-' && strchr("rwxugoa", argv[optind][1]))
		{
			fprintf(stderr,
			        "%s: warning: (portability) Pass -- before a mode with a leading dash(-) to "
			        "separate it from options\n",
			        argv0);
			break;
		}

#ifdef HAS_GETOPT_LONG
		// Need + as first character to get POSIX-style option parsing
		c = getopt_long(argc, argv, "+:cRv", opts, NULL);
#else
		c = getopt(argc, argv, ":cRv");
#endif
		if(c == -1) break;

		switch(c)
		{
		case 'c': // GNU
			opt_c = true;
			break;
		case OPT_REFERENCE: // GNU & NetBSD
			ref_file = optarg;
			break;
		case 'R': // POSIX
			opt_R = true;
			break;
		case 'v': // GNU
			opt_v = true;
			break;
		case ':':
			fprintf(stderr, "%s: error: Missing operand for option: '-%c'\n", argv0, optopt);
			usage();
			return 1;
		case '?': // GNU
			fprintf(stderr, "%s: error: Unrecognised option: '-%c'\n", argv0, optopt);
			usage();
			return 1;
		}
	}

	argc -= optind;
	argv += optind;

	if(ref_file != NULL)
	{
		if(argc < 1)
		{
			fprintf(stderr, "%s: error: Expected >=1 arguments, %d given\n", argv0, argc);
			usage();
			return 1;
		}

		struct stat ref_stat;
		if(stat(ref_file, &ref_stat) != 0)
		{
			fprintf(stderr,
			        "%s: error: Failed to get status from reference file '%s': %s\n",
			        argv0,
			        ref_file,
			        strerror(errno));
			return 1;
		}

		for(int i = 0; i < argc; i++)
		{
			int ret = copy_mode(AT_FDCWD, ref_stat.st_mode, argv[i], argv[i], opt_R);
			if(ret != 0) return ret;
		}

		return 0;
	}

	if(argc < 2)
	{
		fprintf(stderr, "%s: error: Expected >=2 arguments, %d given\n", argv0, argc);
		usage();
		return 1;
	}

	for(int i = 1; i < argc; i++)
	{
		int ret = do_fchmodat(AT_FDCWD, argv[0], argv[i], argv[i], opt_R);
		if(ret != 0) return ret;
	}

	return 0;
}
