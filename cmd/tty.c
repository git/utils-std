// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#include <errno.h>  // errno
#include <stdio.h>  // puts()
#include <unistd.h> // ttyname()

int
main(void)
{
	char *name = ttyname(STDIN_FILENO);

	if(!name)
	{
		if(errno == ENOTTY)
		{
			if(puts("not a tty") < 0)
			{
				return 2;
			}

			return 1;
		}
		else
		{
			perror("tty: error: ttyname");
			return 2;
		}
	}

	if(puts(name) < 0)
	{
		return 2;
	}

	return 0;
}
