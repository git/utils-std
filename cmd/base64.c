// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-FileCopyrightText: 2018 The NetBSD Foundation, Inc.
// SPDX-License-Identifier: MPL-2.0 AND BSD-2-Clause

#define _POSIX_C_SOURCE 200809L
#include <assert.h> /* assert */
#include <ctype.h>  /* isspace */
#include <errno.h>  /* errno */
#include <stdint.h> /* uint8_t */
#include <stdio.h>  /* fopen(), fprintf(), BUFSIZ */
#include <stdlib.h> /* abort */
#include <string.h> /* strerror(), strncmp() */
#include <unistd.h> /* read(), write(), close(), getopt(), opt* */

// 64(26+26+10+2) + NULL
static const char *b64_encmap = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static size_t c_out = 0;
// 76 is lowest of all base64 related RFCs
static long wrap_nl = 76;
const char *argv0 = "base64";

static int
xputc(int c, FILE *stream)
{
	if(fputc(c, stream) == EOF)
	{
		fprintf(stderr, "%s: error: Failed writing: %s\n", argv0, strerror(errno));
		errno = 0;
		return 1;
	}

	int err = ferror(stream);
	if(err != 0)
	{
		fprintf(stderr, "%s: error: Failed writing: %s\n", argv0, strerror(err));
		errno = 0;
		return 1;
	}

	return 0;
}

static inline uint8_t
b64_get(uint8_t pos)
{
	assert(pos <= 64);
	return b64_encmap[pos];
}

static void
b64encode(uint8_t out[4], uint8_t in[3])
{
	out[0] = b64_get(in[0] >> 2);
	out[1] = b64_get((uint8_t)(((in[0] & 0x03) << 4) | (in[1] >> 4)));
	out[2] = b64_get((uint8_t)(((in[1] & 0x0f) << 2) | (in[2] >> 6)));
	out[3] = b64_get(in[2] & 0x3f);
}

static int
encode(FILE *fin, const char *name)
{
	while(1)
	{
		uint8_t obuf[4] = "----";
		uint8_t ibuf[3] = {0, 0, 0};
		uint8_t pad = 0;

		size_t c = 0;
		for(; c < 3; c++)
		{
			int buf = getc(fin);
			if(buf == EOF)
			{
				break;
			}
			ibuf[c] = buf;
		}
		if(c == 0)
		{
			return 0;
		};
		if(ferror(fin))
		{
			fprintf(
			    stderr, "%s: error: Failed reading from file '%s': %s\n", argv0, name, strerror(errno));
			errno = 0;
			return 1;
		}
		for(; c < 3; pad++, c++)
		{
			ibuf[c] = 0;
		}
		assert(c == 3);
		assert(pad <= 3);

		b64encode(obuf, (uint8_t *)ibuf);

		for(; pad > 0; pad--)
		{
			obuf[4 - pad] = '=';
		}

		if(fwrite((char *)obuf, 4, 1, stdout) <= 0)
		{
			fprintf(stderr, "%s: error: Failed writing: %s\n", argv0, strerror(errno));
			errno = 0;
			return 1;
		}
		c_out += 4;

		if(wrap_nl != 0 && (c_out + 4) > wrap_nl)
		{
			c_out = 0;
			if(xputc('\n', stdout) != 0) return 1;
		}

		if(feof(fin))
		{
			if(fflush(stdout))
			{
				fprintf(stderr, "%s: error: Failed writing: %s\n", argv0, strerror(errno));
				errno = 0;
				return 1;
			}
			int err = ferror(stdout);
			if(err != 0)
			{
				fprintf(stderr, "%s: error: Failed writing: %s\n", argv0, strerror(errno));
				return 1;
			}

			return 0;
		}
	}

	abort(); // unreachable
}

// This function is based on NetBSD's code, which contains the following notices:
// Copyright (c) 2018 The NetBSD Foundation, Inc.
//
// This code is derived from software contributed to The NetBSD Foundation
// by Christos Zoulas.
static int
decode(FILE *fin, const char *name)
{
	int c = 0;
	uint8_t out = 0;
	int state = 0;

	while((c = getc(fin)) != EOF)
	{
		if(isspace(c)) continue;

		if(c == '=') break;

		uint8_t b = 65;
		for(uint8_t i = 0; i < 64; i++)
		{
			if(b64_encmap[i] == c)
			{
				b = i;
				break;
			}
		}
		if(b > 64)
		{
			fprintf(stderr, "%s: error: Invalid character '%c'\n", argv0, c);
			return 1;
		}

		//fprintf(stderr, "state: %d | c: %c (%d) | b: %d\n", state, c, c, b);

		switch(state)
		{
		case 0:
			out = (uint8_t)(b << 2);
			break;
		case 1:
			out |= b >> 4;
			if(xputc(out, stdout) != 0) return 1;
			out = (uint8_t)((b & 0xf) << 4);
			break;
		case 2:
			out |= b >> 2;
			if(xputc(out, stdout) != 0) return 1;
			out = (uint8_t)((b & 0x3) << 6);
			break;
		case 3:
			out |= b;
			if(xputc(out, stdout) != 0) return 1;
			out = 0;
			break;
		default:
			abort();
		}
		state = (state + 1) & 3;
	}

	//fprintf(stderr, "[outside] state: %d | c: %c (%d) | out: %d\n", state, c, c, out);

	if(c == '=')
	{
		switch(state)
		{
		case 0:
		case 1:
			fprintf(stderr, "%s: error: Invalid character '%c' (early '=')\n", argv0, c);
			return 1;
		case 2:
			while(isspace(c = getc(fin)))
				;

			if(c != '=')
			{
				fprintf(stderr, "%s: error: Invalid character '%c' (not '=')\n", argv0, c);
				return 1;
			}
			/* fallthrough */
		case 3:
			while(isspace(c = getc(fin)))
				;

			if(c != EOF)
			{
				fprintf(stderr, "%s: error: Invalid character '%c' (not EOF)\n", argv0, c);
				return 1;
			}
			return 0;
		default:
			abort();
		}
	}

	assert(c == EOF || state == 0);

	if(fflush(stdout))
	{
		fprintf(stderr, "%s: error: Failed writing: %s\n", argv0, strerror(errno));
		errno = 0;
		return 1;
	}
	int err = ferror(stdout);
	if(err != 0)
	{
		fprintf(stderr, "%s: error: Failed writing: %s\n", argv0, strerror(err));
		errno = 0;
		return 1;
	}

	return 0;
}

int
main(int argc, char *argv[])
{
	int (*process)(FILE *, const char *) = &encode;

	int ret = 0;

	for(int c = -1; (c = getopt(argc, argv, ":dw:")) != -1;)
	{
		switch(c)
		{
		case 'd':
			process = &decode;
			break;
		case 'w':
			errno = 0;
			char *e = NULL;
			wrap_nl = strtol(optarg, &e, 10);

			// extraneous characters is invalid
			if(e && *e != 0) errno = EINVAL;

			if(errno != 0)
			{
				fprintf(stderr, "%s: error: Option '-w %s': %s\n", argv0, optarg, strerror(errno));
				return 1;
			}
			break;
		case ':':
			fprintf(stderr, "%s: error: Missing operand for option '-%c'\n", argv0, optopt);
			return 1;
		case '?':
			fprintf(stderr, "%s: error: Unrecognised option '-%c'\n", argv0, optopt);
			return 1;
		}
	}

	argc -= optind;
	argv += optind;

	if(argc <= 0)
	{
		ret = process(stdin, "<stdin>");
		goto end;
	}

	for(int argi = 0; argi < argc; argi++)
	{
		if(strncmp(argv[argi], "-", 2) == 0)
		{
			if(process(stdin, "<stdin>") != 0)
			{
				ret = 1;
				goto end;
			}
		}
		else if(strncmp(argv[argi], "--", 3) == 0)
		{
			continue;
		}
		else
		{
			FILE *fin = fopen(argv[argi], "r");
			if(fin == NULL || ferror(fin) != 0)
			{
				fprintf(stderr,
				        "%s: error: Failed opening file '%s': %s\n",
				        argv0,
				        argv[argi],
				        strerror(errno));
				ret = 1;
				goto end;
			}

			if(process(fin, argv[argi]) != 0) ret = 1;

			if(fclose(fin) < 0)
			{
				fprintf(stderr,
				        "%s: error: Failed closing file '%s': %s\n",
				        argv0,
				        argv[argi],
				        strerror(errno));
				ret = 1;
				goto end;
			}
		}
	}

end:
	if(wrap_nl != 0 && c_out > 0)
	{
		printf("\n");
	}

	if(fclose(stdin) != 0)
	{
		fprintf(stderr, "%s: error: Failed closing file <stdin>: %s\n", argv0, strerror(errno));
		ret = 1;
	}

	if(fclose(stdout) != 0)
	{
		fprintf(stderr, "%s: error: Failed closing file <stdout>: %s\n", argv0, strerror(errno));
		ret = 1;
	}

	return ret;
}
