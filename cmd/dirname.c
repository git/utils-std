// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#include <libgen.h> // dirname()
#include <stdio.h>  // puts()
#include <string.h> // strcmp()

int
main(int argc, char *argv[])
{
	if(argc != 2)
	{
		if((argc == 3) && (strcmp(argv[1], "--") == 0))
		{
			argv++;
			argc--;
		}
		else
		{
			fputs("usage: dirname string\n", stderr);
			return 1;
		}
	}

	puts(dirname(argv[1]));
	return 0;
}
