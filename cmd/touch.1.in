.\" utils-std: Collection of commonly available Unix tools
.\" Copyright 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
.\" SPDX-License-Identifier: MPL-2.0
.Dd 2023-06-03
.Dt TOUCH 1
.Os
.Sh NAME
.Nm touch
.Nd change file access and modification times
.Sh SYNOPSIS
.Nm
.Op Fl acfhm
.Op Fl d Ar isotime | Fl t Ar datetime | Fl r Ar ref_file
.Ar file...
.Sh DESCRIPTION
.Nm
changes the date modification and access times on each
.Ar file
it is given.
.Bl -tag -width Ds
.It Fl a
Change the access time, no changes to modification time unless
.Fl m
is also given.
.It Fl c
Do not create
.Ar file .
.It Fl f
Ignored.
coreutils and BusyBox never supported it,
.Ox
removed it in 2005,
.Nx
in 2011,
.Fx
in 2012.
.\" Let's not publicly document too much that illumos still supports it
.It Fl d Ar isotime
include(lib/iso_parse.mdoc)
.It Fl c
Ignored.
.It Fl h
Do not follow symlinks.
.It Fl m
Change the modification time, no changes to access time unless
.Fl a
is also given.
.It Fl t Ar datetime
Use the specified
.Ar datetime
instead of the current time, with the form
.Oo Oo CC Oc Ns YY Oc Ns MMDDhhmm Ns Oo \.SS Oc
where:
.Bl -tag -width _MMDDhhmm_
.It Ql CC
Corresponds to the first 2 digits of the year, aka %C
.It Ql YY
Corresponds to the last 2 digits of the year, aka %y
.It Ql MMDDhhmm
Corresponds to month, day, hours, minutes aka %m%d%H%M
.It Ql .SS
Corresponds to the seconds
.El
.Pp
For example:
.Ql 200306021337.42
.It Fl r Ar ref_file
Use the corresponding times of the file at
.Ar ref_file
instead of the current time.
.El
.Sh EXIT STATUS
.Ex -std
Note: Will exit with failure when
.Fl c
is given but the file doesn't exists.
.Sh SEE ALSO
.Xr stat 1 ,
.Xr futimens 3 ,
.Xr strptime 3
.Sh STANDARDS
.Nm
should be compliant with the
IEEE Std 1003.1-2024 (“POSIX.1”)
specification.
.Pp
.Fl h
and
.Fl f
are extensions.
.Sh AUTHORS
.An Haelwenn (lanodan) Monnier Aq Mt contact+utils@hacktivis.me
