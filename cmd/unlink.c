// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#include <errno.h>
#include <stdio.h>  // fprintf
#include <string.h> // strerror
#include <unistd.h> // unlink

int
main(int argc, char *argv[])
{
	for(int i = 1; i < argc; i++)
	{
		if(unlink(argv[i]) != 0)
		{
			fprintf(stderr, "unlink: error: Failed unlinking '%s': %s\n", argv[i], strerror(errno));
			return 1;
		}
	}

	return 0;
}
