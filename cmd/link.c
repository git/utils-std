// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#define _POSIX_C_SOURCE 200809L
#include <stdio.h>  /* perror, fputs */
#include <unistd.h> /* link */

int
main(int argc, char *argv[])
{
	if(argc != 3)
	{
		fputs("usage: link <reference> <destination>\n", stderr);
		return 1;
	}

	if(link(argv[1], argv[2]) != 0)
	{
		perror("link: error");
		return 1;
	}

	return 0;
}
