// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#define _POSIX_C_SOURCE 200809L
#include <stdio.h>  // fputs, perror
#include <stdlib.h> // getenv

int
main(void)
{
	// Allows to avoid utmp (glibc…), $LOGNAME also being required to match user's login name
	char *logname = getenv("LOGNAME");
	if(logname == NULL)
	{
		fputs("logname: error: $LOGNAME isn't set (didn't login?)\n", stderr);
		return 1;
	}

	if(puts(logname) < 0)
	{
		perror("logname: error: puts");
		return 1;
	}

	return 0;
}
