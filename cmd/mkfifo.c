// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L

#include "../lib/mode.h"

#include <errno.h>
#include <stdio.h>    // fprintf
#include <stdlib.h>   // abort
#include <string.h>   // strerror
#include <sys/stat.h> // mkfifo
#include <unistd.h>   // getopt

mode_t filemask;

static void
usage(void)
{
	fprintf(stderr, "Usage: mkfifo [-m mode] file\n");
}

int
main(int argc, char *argv[])
{
	mode_t mode = 0666;

	const char *errstr = NULL;

	for(int c = -1; (c = getopt(argc, argv, ":m:")) != -1;)
	{
		switch(c)
		{
		case 'm':
			mode = new_mode(optarg, 0666, &errstr);
			if(errstr != NULL)
			{
				fprintf(stderr, "mkfifo: error: Failed parsing mode '%s': %s\n", optarg, errstr);
				return 1;
			}
			break;
		case ':':
			fprintf(stderr, "mkfifo: error: Missing operand for option: '-%c'\n", optopt);
			usage();
			return 1;
		case '?':
			fprintf(stderr, "mkfifo: error: Unrecognised option: '-%c'\n", optopt);
			usage();
			return 1;
		default:
			abort();
		}
	}

	argc -= optind;
	argv += optind;

	if(argc < 1)
	{
		fprintf(stderr, "mkfifo: error: Missing file argument\n");
		usage();
		return 1;
	}

	for(int i = 0; i < argc; i++)
	{
		if(mkfifo(argv[i], mode) != 0)
		{
			fprintf(
			    stderr, "mkfifo: error: Failed creating FIFO at '%s': %s\n", argv[i], strerror(errno));
			return 1;
		}
	}

	return 0;
}
