// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L

#include "../lib/err.h"

#include <errno.h>
#include <pwd.h> // getpwuid
#include <stdio.h>
#include <string.h> // strerror
#include <unistd.h> // geteuid

const char *argv0 = "whoami";

int
main(void)
{
	uid_t euid = geteuid();
	struct passwd *pw = getpwuid(euid);

	if(pw == NULL)
	{
		const char *errstr = strerror(errno);

		if(errno == 0) errstr = "Not Found";

		utils_errx(1, "Failed getting name for user ID %d: %s", euid, errstr);
	}

	puts(pw->pw_name);

	return 0;
}
