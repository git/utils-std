// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>       // fprintf
#include <string.h>      // strerror
#include <sys/utsname.h> // uname

int
main(void)
{
	struct utsname name;
	if(uname(&name) < 0)
	{
		perror("arch: error: Failed to get system name");
		return 1;
	}

	if(puts(name.machine) < 0)
	{
		perror("arch: error: Failed to write machine architecture");
		return 1;
	}

	return 0;
}
