#!/usr/bin/env perl
# Checks if a non-main function wasn't declared static, as ones for executables should be
# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

my $visible = 0;
my $err = 0;

if($#ARGV < 0)
{
	print "Usage: $0 [file.c ...]\n";
	exit 1;
}

while(<>) {
	my $line = $_;

	if($line =~ /^[a-zA-Z0-9_]+\(/) {
		if($line =~ /^main/) { next; }
		if($visible == 0) { next; }

		print "Non-static function ${ARGV}: $line";
		$err = 1;
	} else {
		if($line =~ /^static /) {
			$visible = 0;
		} else {
			$visible = 1;
		}
	}
}

exit $err;
