# SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

# bootstrap.mk targets should be kept simple
lib/fs.o: lib/fs.c lib/fs.h config.h
lib/err.o: lib/err.c lib/err.h
lib/consent.o: lib/consent.c lib/consent.h
lib/tr_str.o: lib/tr_str.c lib/tr_str.h
cmd/cat: cmd/cat.c lib/fs.o
cmd/printf: cmd/printf.c lib/err.o
cmd/rm: cmd/rm.c lib/consent.o
cmd/tr: cmd/tr.c lib/tr_str.o lib/err.o
