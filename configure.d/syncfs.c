// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

// Because that's what our utilities will use :)
#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE // for syncfs

#include <unistd.h> // syncfs

int
main(void)
{
	int fd = 0;
	return syncfs(fd);
}
