// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 202405L

#include <getopt.h> // getopt_long
#include <stdio.h>  // puts
#include <stdlib.h> // abort

int
main(int argc, char *argv[])
{
	int optidx = 0;
	char *file = NULL;

	// clang-format off
	static struct option opts[] = {
		{"help", no_argument, 0, 'h'},
		{"file", required_argument, 0, 'f'},
		{0, 0, 0, 0}
	};
	// clang-format on

	for(int c = -1; (c = getopt_long(argc, argv, ":hf:", opts, &optidx)) != -1;)
	{
		switch(c)
		{
		case 'h':
			puts("Help!");
			return 1;
			break;
		case 'f':
			file = optarg;
			break;
		default:
			abort();
		}
	}

	puts(file);

	return 0;
}
