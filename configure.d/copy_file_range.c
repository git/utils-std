// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

// For copy_file_range
#define _GNU_SOURCE     // musl, glibc
#define _DEFAULT_SOURCE // FreeBSD

#include <stdint.h> // SIZE_MAX
#include <sys/types.h>
#include <unistd.h>

int
main(void)
{
	int fd_in = 0;
	int fd_out = 1;
	off_t in_pos = 0, out_pos = 0;

	ssize_t ret = copy_file_range(fd_in, &in_pos, fd_out, &out_pos, SIZE_MAX, 0);

	return ret > 0;
}
