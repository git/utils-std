// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: 0BSD

int foo(int c);

int
foo(int c)
{
	return c;
}
