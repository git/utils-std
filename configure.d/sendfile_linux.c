// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

// Because that's what our utilities will use :)
#define _POSIX_C_SOURCE 200809L

#include <sys/sendfile.h>

int
main(void)
{
	int fd_in = 0;
	int fd_out = 1;
	off_t *offset = (off_t)0;
	size_t len = 42;

	ssize_t ret = sendfile(fd_out, fd_in, offset, len);

	return ret > 0;
}
