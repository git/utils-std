// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

// Because that's what join uses :)
#define _BSD_SOURCE

#include <stdio.h>

int
main(void)
{
	FILE *stream = stdin;
	size_t len = 0;

	char *out = fgetln(stream, &len);

	return out != NULL;
}
