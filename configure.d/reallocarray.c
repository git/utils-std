// utils-std: Collection of commonly available Unix tools
// SPDX-FileCopyrightText: 2017 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 202405L

#include "../lib/reallocarray.h"

#include <stdlib.h> // reallocarray

int
main(void)
{
	char *arr = NULL;
	arr = reallocarray(&arr, 69, sizeof(*arr));

	return arr != NULL;
}
